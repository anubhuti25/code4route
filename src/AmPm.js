import React, { PropTypes } from 'react';

function AmPm(props) {
  const { e } = props;

  var hours = e.slice(0,e.indexOf(':'));
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  var minutes=e.slice(e.indexOf(':')+1,e.lastIndexOf(':'))
  var el = hours + ':' + minutes + ' ' + ampm;

  if (!e || !e.length) {
    return false;
  }

  return (
    <span>{el}</span>
  );
}

AmPm.propTypes = {
  e : PropTypes.string,
};

export default AmPm;
