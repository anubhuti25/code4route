import React from 'react';
import ReactDOM from 'react-dom';
import 'isomorphic-fetch';
import App from './App.js';
import Owner from './Owner.js';
import Add from './Add.js';
import Deleter from './Deleter';
import Display from './display/Display';
import ResPage from './ResPage';
import User from './User.js';
import VerifyUser from './VerifyUser.js';
import { Router, Route, hashHistory } from 'react-router';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import Listmapview from './Listmapview'

import './index.css';

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={App}/>
    <Route path="/Owner" component={Owner}>
        <Route path="/Owner/Add" component={Add}/>
        <Route path="/Owner/Delete" component={Deleter}/>
        <Route path="/Owner/Delete/:textvalue" component={ResPage}/>
    </Route>

    <Route path = "/User" component = {User} />

    <Route path = "/Newr" component = {App} />
    <Route path = "/contact" component = {App} />
    <Route path = "/HomeMap" component = {App} />

    <Route path="/VerifyUser" component = {VerifyUser} >
      <Route path = "/VerifyUser/:uname/:uval" component = {VerifyUser} />
    </Route>

    <Route path = "/Listmapview/:rests/:value" component = {Listmapview} />

    <Route path="/Display"  component={Display}>
      <Route path="/Display/:resRet"  component={Display}/>
    </Route>
  </Router>
), document.getElementById('root'));
