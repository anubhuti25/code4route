import React, { Component } from 'react';
import './App.css';

class DropDemo extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
    this.state={
      results: [],
    }
    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch() {
    var x = document.getElementById("myFile"+(this.props.menu1?"1":""));
    var form = new FormData();
    var that=this;
    var menus="";
    if(this.props.menu1){
      menus="menu";
    }
    console.log(menus);
    for (var i = 0; i < x.files.length; i++) {
      var file = x.files[i];
      form.set('picture',file);
      this.props.handleFiles(form,menus);
    }
  }

  render() {

    return (
      <div className="DropDemo">
        <form id="login-form">
          <input type="file" id={"myFile"+(this.props.menu1?"1":"")} name="picture" multiple="multiple" accept=".png" onChange={this.handleSearch}/>
        </form>
      </div>
    );
  }
}

export default DropDemo;
