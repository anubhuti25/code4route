import React, { Component } from 'react';
import Search from './Search';
import './Filters.css';
import { Link , hashHistory } from 'react-router';

class Filters extends Component {
  constructor() {
    super();

    this.state = {
      rests : [],
      value : [],
      cuisine : [],
      area : [],
      change : false
    };
    this.hc = this.hc.bind(this);
    this.sc = this.sc.bind(this);
    this.remove = this.remove.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
    this.addFilter = this.addFilter.bind(this);
  }

  componentDidMount() {

    document.getElementById( 'refreshAll' ).style.visibility = "hidden";

    fetch( 'http://localhost:9000/cuisines' ,
    {
      method : 'GET',
      headers :{
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      },
     })
     .then( response => response.json() )
     .then(( json ) => this.setState({ cuisine : json }));

     fetch( 'http://localhost:9000/areas' ,
     {
       method : 'GET',
       headers :{
         "Content-Type" : "application/json",
         "Accept" : "application/json"
       },
      })
      .then( response => response.json() )
      .then(( json ) => this.setState({ area : json }));

   }

   hc(e){

     var flag;
     if( e.currentTarget.id == "cuisineBtn" )
        flag = "cuisineModal";
      else if( e.currentTarget.id == "areaBtn" )
        flag = "areaModal"

      document.getElementById(flag).style.display = "block";
   }

   sc(e){

    var flag;
    if(e.currentTarget.id=="closeCusine")
      flag="cuisineModal";
    else if(e.currentTarget.id=="closeArea")
      flag="areaModal";

    document.getElementById(flag).style.display ="none";
   }

   handleRefresh(e){

     hashHistory.push( '/Listmapview/1/1' );
     window.location.reload();
   }

   remove(e){
     var element=e.currentTarget.id;
     var index=element.lastIndexOf("1");
     var slicePortion="close"
     if(index==element.length-1)
        slicePortion="close1"
     element=element.substring(0,e.currentTarget.id.lastIndexOf(slicePortion));
     var class1=e.currentTarget.className.substr(12);

     var val=element;

     if (class1=="cost") {

       var val2=[parseInt(val)+250];
       val2.unshift(parseInt(val)-250);
       val=val2;

     }

     if(class1=="area"||class1=="cuisine"){
        document.getElementById(class1+"Modal").style.display ="none";
        document.getElementById(class1+"Lbl").innerHTML='';
        document.getElementById(element+"close1").style.display="none";
        e.currentTarget.setAttribute("id",(class1=="area"?"aLbl":"cLbl"));
      }

      if(class1=="time"){
        val=1;
      }

     this.props.removeFilter(val,class1);
     document.getElementById(element).checked=false;
     document.getElementById(element+'Label').style.color="black";
     document.getElementById(element+'Label').style.fontWeight="normal";
     document.getElementById(element+"close").style.display="none";

     var flag = true;
     Array.from( document.getElementsByClassName( 'fa-times' )).map( function( j , i ){
       if( j.style.display == "inline-block" ){
         flag = false;
       }
     });

     if( flag ){
       document.getElementById( 'refreshAll' ).style.visibility = "hidden";
     }
   }

   addFilter(e){

     var element=e.currentTarget.name;
     var modal;
     var removeBtn;
     var fil=element.substr(0,element.lastIndexOf("Check"));;
     var fil1=false;
     var val,v;
     var val2;
     var oldVal='';

     var checkColor=Array.from(document.getElementsByName(element));

     checkColor.map(function(j,i){
       document.getElementById(j.id+'Label').style.color="black";
       document.getElementById(j.id+'Label').style.fontWeight="normal";

     })

     val=e.currentTarget.value;

     document.getElementById('refreshAll').style.visibility = "visible";

     var checkStatus=Array.from(document.getElementsByClassName(fil));
     checkStatus.map(function(j,i){

       if(j.style.display=="inline-block"){

        j.style.display="none";
        if(i==checkStatus.length-1){
          if(fil=="area")
            j.setAttribute("id","aLbl");
          else if (fil=="cuisine") {
            j.setAttribute("id","cLbl")
          }
        }
       }
     });

     if(document.getElementById(val).checked){
     if(element=="areaCheck"){

       fil1=true;
       modal="areaModal";
       v=val;
       removeBtn=val+'close';
       document.getElementById(fil+"Lbl").innerHTML=val;
       document.getElementById(fil+"Lbl").style.color="#06a50a";
       document.getElementById(fil+"Lbl").style.fontWeight="bold";
       document.getElementById("aLbl").style.display="inline-block";
       document.getElementById("aLbl").setAttribute("id",val+"close1");
       document.getElementById(val+'close').style.display="inline-block";
     }
     else if (element=="cuisineCheck") {

       v=val;
       modal="cuisineModal"
       removeBtn=val+'close';
       fil1=true;
       document.getElementById(fil+"Lbl").innerHTML=val;
       document.getElementById("cLbl").style.display="inline-block";
       document.getElementById(fil+"Lbl").style.color="#06a50a";
       document.getElementById(fil+"Lbl").style.fontWeight="bold";
       document.getElementById("cLbl").setAttribute("id",val+"close1");
       document.getElementById(val+'close').style.display="inline-block";
     }
     else if (element=="costCheck") {

       v=val;
       document.getElementById(val+'close').style.display="inline-block";
       val2=[parseInt(val)+250];
       val2.unshift(parseInt(val)-250);
       val=val2;

     }
     else if (element=="sortCheck") {

       v=val;
       document.getElementById(val+'close').style.display="inline-block";
       console.log("sorting");
     }

     else if (element=="ratingCheck") {

       v=val;
       console.log("rating");
       document.getElementById(val+'close').style.display="inline-block";
     }
     else if (element=="timeCheck") {

       v=val;
       document.getElementById(val+'close').style.display="inline-block";
       val=1;
     }

     this.setState({rests:fil,value:val,change:true})
     document.getElementById(v+'Label').style.color="#06a50a"
     document.getElementById(v+'Label').style.fontWeight="bold"
     modal?document.getElementById(modal).style.display ="none":'';
   }

   }


   componentDidUpdate(){
      if(this.state.change){
        this.setState({change:false});
        {this.props.searchFilters(this.state)};
      }
   }

   clearAllFilters(){
     var elements=Array.from(document.getElementsByClassName('fa-times'));
     elements.map(function(j,i){
       var ele=j.id.substring(0,j.id.lastIndexOf('close'));
        if(j.id.lastIndexOf('close')!=-1){
          if(j.id.lastIndexOf('close1')==-1)
              document.getElementById(ele).checked=false;
          else {
            var class2=j.className.substr(12);
            document.getElementById(class2+'Lbl').innerHTML='';
          }
        }
       j.style.display="none";
     });
   }

  render()
  {

    if(this.props.clearAll){
      this.clearAllFilters();
    }

    var cuisneList=this.state.cuisine;
    if(cuisneList){
      cuisneList=cuisneList.map(function(j,i){

        return(
          <div className="col-xs-4" key={i}>
              <label className="check" id={j.cuisine+"Label"}>
                  <input type="radio" name="cuisineCheck" onChange={this.addFilter} value={j.cuisine} id={j.cuisine}/>
                  {j.cuisine}
              </label> <i name="cost" className="fa fa-times cuisine" onClick={this.remove} id={j.cuisine+"close"} />
          </div>
        );
      },this);
    }

    var areaList=this.state.area;
    if(areaList){
      areaList=areaList.map(function(j,i){
        // console.log(j.area)
        return(
          <div className="col-xs-4" key={i}>
              <label className="check" id={j.area+"Label"}>
                  <input type="radio" name="areaCheck" onClick={this.addFilter} value={j.area} id={j.area}/>
                  {j.area}
              </label> <i name="cost" className="fa fa-times area" onClick={this.remove} id={j.area+"close"} />
          </div>
        );
      },this);
    }

    return(
      <div className="Filters">
        <div className = "visible-xs">
          <Search search = {this.props.searchFilters} filtMob = {true}/>
        </div>
        <form id="form1">

          <h4 className="heading">
              Filters
              <Link id="refreshAll" onClick = { this.handleRefresh } >
                  Remove All
              </Link>
          </h4>

          <div id="oneLabel" className="subheadings">
              <label className="check" >
                  <input type="radio" name="timeCheck" onChange={this.addFilter} value="one" id='one'/>
                  Now Open
              </label> <i name="time" className="fa fa-times time" onClick={this.remove} id="oneclose" />
          </div>


          <div className="subheadings">
              Cost for two(<i className="fa fa-rupee"/>)
          </div>

          <div id="250Label">
              <label className="check" >
                  <input type="radio" name="costCheck" onChange={this.addFilter} value="250" id='250'/>
                  Below 500
              </label> <i name="cost" className="fa fa-times cost" onClick={this.remove} id="250close" />
          </div>

          <div>
              <label className="check" id="750Label">
                  <input type="radio" name="costCheck" onChange={this.addFilter} value="750" id='750'/>
                  500 - 1000
              </label>  <i className="fa fa-times cost" onClick={this.remove} id="750close" />
          </div>

          <div>
              <label className="check" id="1250Label">
                  <input type="radio" name="costCheck" onChange={this.addFilter} value="1250" id='1250'/>
                  1000-1500
              </label> <i className="fa fa-times cost" onClick={this.remove} id="1250close" />
          </div>

          <div>
              <label className="check" id="1750Label">
                  <input type="radio" name="costCheck" onChange={this.addFilter} value="1750" id='1750'/>
                  1500-2000
              </label> <i className="fa fa-times cost" onClick={this.remove} id="1750close" />
          </div>

          <div>
              <label className="check" id="2250Label">
                  <input type="radio" name="costCheck" onChange={this.addFilter} value="2250" id='2250'/>
                  Above 2000
              </label> <i className="fa fa-times cost" onClick={this.remove} id="2250close" />
          </div>

          <div>

              <hr />

              <button type="button" id="cuisineBtn" className="btn btn-link subheadings" onClick={this.hc}>
                  Cuisine <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
              </button>

              <div id="cuisineModal" className="modal">
                  <div className="modal-dialog">
                      <div className="modal-content">
                          <div className="modal-header">
                              <button type="button" className="close" id="closeCusine" onClick={this.sc}>&times;</button>
                              <h2 className="modal-title">Select a Cuisine</h2>
                          </div>

                          <div className="modal-body">
                                <div className="container-fluid">
                                      <div className="row">
                                          {cuisneList}
                                      </div>
                                  </div>

                          </div>
                      </div>
                  </div>
              </div>

              <label className="check" id="cuisineLbl">

              </label> <i className="fa fa-times cuisine" onClick={this.remove} id="cLbl" />
          </div>

          <div>

              <hr />

              <button type="button" id="areaBtn" className="btn btn-link subheadings" onClick={this.hc}>
                  Area <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
              </button>

              <div id="areaModal" className="modal">
                  <div className="modal-dialog">
                      <div className="modal-content">
                          <div className="modal-header">

                              <button type="button" className="close" id="closeArea" onClick={this.sc}>&times;</button>
                              <h2 className="modal-title">Select a Location</h2>
                          </div>

                          <div className="modal-body">

                                  <div className="container-fluid">
                                      <div className="row">
                                          {areaList}
                                      </div>
                                  </div>

                          </div>
                      </div>
                  </div>
              </div>
              <label className="check" id="areaLbl">
              </label> <i className="fa fa-times area" onClick={this.remove} id="aLbl" />
          </div>

          <hr />

          <div className="subheadings">
              Sort By
          </div>

          <div>
              <label className="check" id="CostDLabel">
                  <input type="radio" name="sortCheck" onChange={this.addFilter} value="CostD" id="CostD"/>
                  Cost <i className="fa fa-sort-numeric-desc" aria-hidden="true"></i>
              </label> <i className="fa fa-times sort" onClick={this.remove} id="CostDclose" />
          </div>

          <div>
              <label className="check" id="CostALabel">
                  <input type="radio" name="sortCheck" onChange={this.addFilter} value="CostA" id="CostA"/>
                  Cost <i className="fa fa-sort-numeric-asc" aria-hidden="true"></i>
              </label> <i className="fa fa-times sort" onClick={this.remove} id="CostAclose" />
          </div>

          <div>
              <label className="check" id="RecentLabel">
                  <input type="radio" name="sortCheck" onChange={this.addFilter} value="Recent" id="Recent"/>
                  Recent
              </label> <i className="fa fa-times sort" onClick={this.remove} id="Recentclose" />
          </div>

          <div>
              <label className="check" id="RatingLabel">
                  <input type="radio" name="sortCheck" onChange={this.addFilter} value="Rating" id="Rating"/>
                  Rating
              </label> <i className="fa fa-times sort" onClick={this.remove} id="Ratingclose" />
          </div>

          <hr />

          <div className="subheadings">
              Rating
          </div>

          <div>
              <label className="check" id="2Label">
                  <input type="radio" name="ratingCheck" onChange={this.addFilter} value="2" id="2"/>
                  2 and above
              </label> <i className="fa fa-times rating" onClick={this.remove} id="2close" />
          </div>

          <div>
              <label className="check" id="3Label">
                  <input type="radio" name="ratingCheck" onChange={this.addFilter} value="3" id="3"/>
                  3 and above
              </label> <i className="fa fa-times rating" onClick={this.remove} id="3close" />
          </div>

          <div>
              <label className="check" id="4Label">
                  <input type="radio" name="ratingCheck" onChange={this.addFilter} value="4" id="4"/>
                  4 and above
              </label> <i className="fa fa-times rating" onClick={this.remove} id="4close" />
          </div>

        </form>
      </div>
    );
  }
}

export default Filters;
