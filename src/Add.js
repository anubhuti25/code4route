import Owner from './Owner';
import 'bootstrap/dist/css/bootstrap.css';
import React, { Component } from 'react';
import { Link , hashHistory} from 'react-router';
import './Add.css';
import MapSample from './MapSample';
import DropDemo from './DropDemo';
import Uploads from './Uploads';
import headTags from 'head-tags';

var num=0;
class Add extends Component {
  constructor() {
    super();
	  this.state={
      lat:'',
      lng:'',
      cuisine:[],
      area:[],
      menus:[],
      files:[],
      areaSelected:''
    }

    this.handleClick=this.handleClick.bind(this);
    this.removeBtn=this.removeBtn.bind(this);
    this.handleRegister=this.handleRegister.bind(this);
    this.getLatLng=this.getLatLng.bind(this);
    this.handleFileUpload=this.handleFileUpload.bind(this);
    this.handleFileDelete=this.handleFileDelete.bind(this);
    this.handleAreas=this.handleAreas.bind(this);
    this.validateText = this.validateText.bind(this);
  }

  componentDidMount(){

    fetch('http://localhost:9000/cuisines',
    {
      method: 'GET',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
     })
     .then(response=>response.json())
     .then((json)=>this.setState({cuisine:json}));

     fetch('http://localhost:9000/areas',
     {
       method: 'GET',
       headers:{
         "Content-Type":"application/json",
         "Accept":"application/json"
       },
      })
      .then(response=>response.json())
      .then((json)=>this.setState({area:json}));
  }

  getLatLng(l1,l2){
    this.setState({lat:l1,lng:l2});
  }

  handleFileDelete(e,l){
    var f=[];
    if(l=="menu"){
      f=this.state.menus
    }
    else {
      f=this.state.files;
    }

    var deletedFile=f[e];
    console.log("deleted image is"+f[e]);

    f[e]=undefined;

    fetch(deletedFile,
    {
      method: 'DELETE',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>console.log(response.status))
     .then(function(e){document.getElementById('myFile').value="";});

     if(l=="menu"){
       this.setState({menus:f});
     }
     else {
       this.setState({files:f});
     }
  }


  handleFileUpload(e,ll){
    var that=this;
    fetch("http://localhost:9000/images", {
      method: "POST",
      body: e
    })
    .then(response=>response.json())
    .then(function(json){
      var a=[];
      if(ll=="menu")
          a=[...that.state.menus];
      else {
        a=[...that.state.files];
      }
      a.push(json);

      if(ll=="menu"){
        that.setState({menus:a});
      }

      else {
        that.setState({files:a});
      }
    });

  }

  validateText(val,id){
    if(!document.getElementById(val).value){
      document.getElementById(id).innerHTML = 'Please enter '+val;
      return true;
    }
	document.getElementById(id).innerHTML = '';
    return false;
  }

  handleRegister(e){

    var li=document.getElementById("addLink").href="http://localhost:8081/#/Owner/";
    console.log(this.state.areaSelected);
    // Get the selected highlights

    var highlights1=[];
    for(var i=0;i<=num;i++){
      if(document.getElementById('text'+i)){
        highlights1.push(document.getElementById('text'+i).value);
      }
    }

    //Get all the cuisines
    var cuisine=this.state.cuisine;
    var cuisineSelected=[];
    for(var i=0;i<cuisine.length;i++){
      if(document.getElementById(cuisine[i].cuisine).checked){
        cuisineSelected.push(cuisine[i]);
      }
    }

    //validations
    var valFlag = false;
    if(this.validateText('Name','valName'))
      valFlag = true;
    if(this.validateText('address','valAdd'))
      valFlag = true;
    if(!this.state.areaSelected.length)
    {
      valFlag = true;
      document.getElementById('valArea').innerHTML = 'Please select area';
    }
	else{
		document.getElementById('valArea').innerHTML = '';
	}
    if(cuisineSelected.length == 0){
      valFlag = true;
      document.getElementById('valCuisine').innerHTML = 'Please select cuisine';
    }
	else{
		document.getElementById('valCuisine').innerHTML = '';
	}
    if(this.validateText('cost','valCost'))
      valFlag = true;
    if(this.validateText('openTime','valOtime'))
      valFlag = true;
    if(this.validateText('closeTime','valCtime'))
      valFlag = true;
    if(highlights1.length == 0){
      valFlag = true;
      document.getElementById('valHigh').innerHTML = 'Please enter highlights';
    }
	else{
		document.getElementById('valHigh').innerHTML = '';
	}
    if(this.validateText('phn','valPhone'))
      valFlag = true;
    if(this.validateText('description','valDesc'))
      valFlag = true;
    if(!this.state.files.length){
      console.log('photo not valid');
      valFlag = true;
      document.getElementById('valPhotos').innerHTML = 'Please upload restaurant photos';
    }
	else{
		document.getElementById('valPhotos').innerHTML = '';
	}
    if((!this.state.lat) || (!this.state.lng)){
      valFlag = true;
      document.getElementById('valLoc').innerHTML = 'Please give location';
    }
	else{
		document.getElementById('valLoc').innerHTML = '';
	}
    

    if(valFlag){
      console.log(valFlag);
      return;
    }

    var files1 = this.state.files;
    var files2 = [];

    for(var k=0 ; k<files1.length ; k++){
      if(files1[k]){
        files2.push(files1[k])
      }
    }

    var menus1 = this.state.menus;
    var menus2 = [];

    for(var m=0 ; m<menus1.length ; m++){
      if(menus1[m]){
        menus2.push(menus1[m])
      }
    }


    //converting form data to json
    var form=JSON.stringify({
      name:document.getElementById('Name').value,
      address:document.getElementById('address').value,
      cuisine:cuisineSelected,
      cost:document.getElementById('cost').value,
      openTime:document.getElementById('openTime').value+":00",
      closeTime:document.getElementById('closeTime').value+":00",
      phn:document.getElementById('phn').value,
      fbUrl:document.getElementById('fbUrl').value,
      description:document.getElementById('description').value,
      latitude:this.state.lat,
      longitude:this.state.lng,
      highlights:highlights1,
      photosPath:files2,
      menuPath:menus2,
      area:this.state.areaSelected,
      homePageUrl:document.getElementById('hmUrl').value
    });

    //calling POST for add restaurant REST api
    fetch('http://localhost:9000/restaurants',
    {
      method: 'POST',
      headers:{
        "Authorization":"Bearer "+localStorage.auth_token,
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
      body:form
     })
     .then(function(response){
       if( response.status != 200 ){
         localStorage.auth_token = '';
         localStorage.role = "";
         localStorage.uname = "";
         window.alert( "Time period expired. Please login again" );
       }
       window.location.reload();
       window.location.replace(li);
     });
  }

  //adding highlights
  handleClick(e){
    num++;
    //creating a new textbox and button
    var element = document.createElement("input");
    var element1=document.createElement("button");
    var element2=document.createElement("span");

    //setting attributes for textbox
    element.setAttribute("type","text");
    element.setAttribute("className","form-control "+num);
    element.setAttribute("id","text"+num);

    //setting attributes for minus button
    element1.setAttribute("className","btn btn-default minus");
    element1.setAttribute("id",num);
    element1.setAttribute("style","background-color: #fed136;border-color:#fed136;color: white;");
    element1.onclick=this.removeBtn;

    //defining the minus icon
    element2.className="glyphicon glyphicon-minus";

    //appending elements to the form
    var foo = document.getElementById("tb");
    foo.appendChild(element);
    foo.appendChild(element1);

    //appending minus icon to the button
    foo = document.getElementById(num);
    foo.appendChild(element2);
  }

  removeBtn(e){
    //remove the minus button that was clicked and its corresponding textbox
    var foo=document.getElementById("tb");
    foo.removeChild(e.currentTarget);
    foo.removeChild(document.getElementById("text"+e.currentTarget.id));
  }

  handleAreas(e){

    document.getElementById('arealist').innerHTML=e.currentTarget.id;
    this.setState({areaSelected:e.currentTarget.id});

  }

  render() {

	var resList=this.state.resRet;
    // this.sortResults(this.state.sorting)
    // console.log(this.state.filters);
	const options = {
		identifyAttribute: 'head-manager'
	  }
	  const headTitle = headTags({
		titleTemplate() {
		return 'Register Restaurant'
	  },
	  meta: [
		{charset: 'utf-8'}
	  ]
	  }, options)

	  headTitle.title.toString()
	  headTitle.title.mount()

    var cuisines=this.state.cuisine;
    var taskList1=cuisines.map(function(j,i){
    var rObj=j.cuisine;
      return (
        <div className="col-sm-6 col-md-6 col-lg-4" key={i.toString()}>
            <div className="checkbox">
                <label>
                    <input type="checkbox" id={rObj} value={rObj} />{rObj}
                </label>
            </div>
        </div>
      );
    });

    var areas = this.state.area;
    // console.log(areas);
    var areaList = areas.map(function(j,i){
      return(
        <li key={i}><Link className = "form-control" onClick={this.handleAreas} id={j.area} to={hashHistory.getCurrentLocation().pathname}>{j.area}</Link></li>
      );
    },this)


    return (
      <div className="container detail" id="cont">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 detail hello" id="hh">
            <form className="form-horizontal" role="form" id="detForm">
                <h2>Restaurant Details</h2>
                <div className="form-group">
                    <label htmlFor="firstName" className="col-sm-3 control-label">Name:</label>
                    <div className="col-sm-9">
                        <input type="text" id="Name" placeholder="Restaurant Name" className="form-control" required />
                    </div>
                </div>
                <p id="valName"></p>
                <div className="form-group">
                    <label htmlFor="address" className="col-sm-3 control-label">Address:</label>
                    <div className="col-sm-9">
                        <input type="text" id="address" placeholder="Address" className="form-control" required />
                    </div>
                </div>
                <p id="valAdd"></p>
                <div className="form-group">
                    <label htmlFor="Area" className="col-sm-3 control-label">Select Area:</label>
                    <div className="col-sm-9">
                      <div className="dropdown">
                        <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                          <span id="arealist" >Area</span> <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu">
                            {areaList}
                        </ul>
                      </div>
                    </div>
                </div>
                <p id="valArea"></p>
                <div className="form-group">
                  <label id="ca" className=" control-label col-sm-3" required>Cuisine: </label>
                </div>
                <p id="valCuisine"></p>

                <div id="cuisine1">
                    {taskList1}
                    </div>
                <div className="form-group clear">
                    <label className="col-sm-3 control-label ">Cost:</label>
                    <div className="col-sm-9">
                        <input type="text" id="cost" placeholder="Approx for 2" className="form-control" required/>
                    </div>
                </div>
                <p id="valCost"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Opening Time:</label>
                    <div className="col-sm-9">
                        <input type="time" id="openTime" className="form-control" required/>
                    </div>
                </div>
                <p id="valOtime"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Closing Time:</label>
                    <div className="col-sm-9">
                        <input type="time" id="closeTime" className="form-control" required/>
                    </div>
                </div>
                <p id="valCtime"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Highlights (click on + to add ):</label>
                    <button type="button" className="btn btn-default" onClick={this.handleClick} id="highBtn" >
                        <span className="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                    <div className="col-md-6" id="tb">

                    </div>
                </div>
                <p id="valHigh"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Phone Number:</label>
                    <div className="col-sm-9">
                        <input type="number" id="phn" className="form-control" required/>
                    </div>
                </div>
                <p id="valPhone"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Facebook page:</label>
                    <div className="col-sm-9">
                        <input type="url" id="fbUrl" placeholder=" Enter Facebook page Url" className="form-control" required/>
                    </div>
                </div>
				<div className="form-group">
                    <label className="col-sm-3 control-label">Home page:</label>
                    <div className="col-sm-9">
                        <input type="url" id="hmUrl" placeholder=" Enter Home page Url" className="form-control"/>
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Description:</label>
                    <div className="col-sm-9">
                        <input type="text" className="form-control" placeholder="Your Message " id="description" required/>
                </div>
                <p id="valDesc"></p>
            </div>
          </form>
            </div>
                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 hello">
                  <div className="form-group">
                        <div className="col-sm-11  detail bg">
                          <h2 className="head1">Upload Photos</h2>
                          <p id="valPhotos"></p>
                          <DropDemo handleFiles={this.handleFileUpload}/>
                          <Uploads images1={this.state.files} handleFiles={this.handleFileDelete}/>
                        </div>
                    </div>
					<div className="form-group">
                        <div className="col-sm-11  detail bg">
                          <h2 className="head1">Upload Menu Photos</h2>
                          <DropDemo menu1={true} handleFiles={this.handleFileUpload}/>
                          <Uploads menu1={true} images1={this.state.menus} handleFiles={this.handleFileDelete}/>
                        </div>
                    </div>
                    <div className="map col-sm-11  detail">
                      <h2>Select your location</h2>
                    <MapSample
                      area1={this.state.areaSelected} add1={true} onMChange={this.getLatLng}
                    />
                    <p id="valLoc"></p>
                  </div>
                </div>
              </div>
              <div className="form-group row">
                <div className="col-sm-6 col-sm-offset-3">
                  <Link id="addLink" to={hashHistory.getCurrentLocation().pathname}><button type="submit" className="btn btn-primary btn-block" id="regButton" onClick={this.handleRegister}>Register Restaurant</button></Link>
                </div>
              </div>
      </div>
);
}
}

export default Add;
