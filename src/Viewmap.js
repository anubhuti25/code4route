import React, { Component } from 'react';
import './ViewMap.css'
var map;
var bounds = new google.maps.LatLngBounds();

var p1;
class Viewmap extends Component {
  constructor() {
    super();
    this.state = {
      restaurants : []
    }
    this.handleMap = this.handleMap.bind(this);
    this.handleOpenClose=this.handleOpenClose.bind(this);
  }

  handleOpenClose(d){
      var parts = d.split(/:|\s/),
      date  = new Date();
      date.setHours(+parts.shift());
      date.setMinutes(+parts.shift());
      return date;
  }

  componentWillReceiveProps( nextProps ){

    var r = nextProps.restlist;
    this.setState({ restaurants : r });
    map = new google.maps.Map( this.refs.map , {
      scrollwheel: false
    });
  }

  handleMap(pos){
    var restlist = this.state.restaurants;
    var infowindw = new google.maps.InfoWindow();
    if( restlist.length != 0 ){
      restlist.map( function( j , i ){

        var myLatlong = new google.maps.LatLng( j.latitude , j.longitude );

        map.setCenter( myLatlong );
        var marker = new google.maps.Marker({
          position : myLatlong,
          // animation: google.maps.Animation.DROP,
          map : map
        });

        if(this.props.hovCoord){
          if(this.props.hovCoord.lat() == myLatlong.lat() ) {
            marker.setAnimation(null);
            marker.setAnimation(google.maps.Animation.BOUNCE);
          }
        }
        else{
          marker.setAnimation(google.maps.Animation.DROP);
        }

        bounds.extend( myLatlong );
        map.fitBounds( bounds );

        var today = new Date();
        var startDate;
        var endDate;
        {startDate = this.handleOpenClose(j.openTime)};
        {endDate   = this.handleOpenClose(j.closeTime)};

        if(endDate.getHours()<startDate.getHours())
            endDate.setHours(endDate.getHours()+24);

        var open = today <= endDate && today >= startDate ? 'open' : 'closed';

        marker.addListener( 'mouseover' , function() {

          var photo= j.photosPath?JSON.stringify(j.photosPath[0]):'';

          //directions for route
          var directionsService = new google.maps.DirectionsService;
          var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers:true });

          var dist;

          directionsDisplay.setMap(map);
          //route determination
          directionsService.route({
              origin: pos,
              destination: myLatlong,
              travelMode: 'DRIVING'
          }, function(response, status) {
              if (status === 'OK') {
                  dist = response.routes[0].legs[0].distance.text;
                  var time = response.routes[0].legs[0].duration.text;
                  //setting info window content
                  var contentString = '<div id="iw-container" >'+
                      '<img id="listImg" src='+photo+' />'+
                      '<div class="iw-content">'+
                      '<div class="iw-title">'+
                      '<a href="http://localhost:8081/#/Display/'+j.id+'">'+j.name+'</a>'+
                      '</div>'+
                      '<div>'+j.area +'&nbsp;<span id="'+open+'">'+open+'</span>'+'</div>'+
                      '<div>'+'<span class="boldin">Distance: </span>'+dist+'</div>'+
                      '<div>'+'<span class="boldin">Duration: </span>'+time+'&nbsp;<i class="fa fa-car"></i></div>'+
                      '</div>'+
                      '</div>';
                  infowindw.setContent(contentString);
                  infowindw.open(map,marker);

              } else {
                  console.log('Directions request failed due to ' + status);
              }
          });
          //remove directions on closing the infowindow
          google.maps.event.addListener(infowindw,'closeclick',function(){
              map.fitBounds( bounds );
              directionsDisplay.setMap(null);
          });
        });
      },this);
    }
  }

  render() {

    // console.log(geoip_latitude());
    var restlist = this.state.restaurants;
    var infowindw = new google.maps.InfoWindow();
    var that=this;

    if ( navigator.geolocation ) {
      navigator.geolocation.getCurrentPosition( function( position ) {
        var pos = {
          lat : position.coords.latitude,
          lng : position.coords.longitude
        };

        p1 = new google.maps.LatLng(pos.lat,pos.lng);
        p1 = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );
        that.handleMap(p1);

    }, function() {
      var myLatlng1 = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );
      that.handleMap(myLatlng1);
      // handleLocationError( true , infoWindow , map.getCenter());
      });
    }

    else {

      handleLocationError( false , infoWindow , map.getCenter());
    }

    return (
      <div id = "HomeMap">
        <h1 className = "centr wbg" > View On Map </h1>
        <div ref = "map" style = {{height : 500}}></div>
      </div>
    );
  }
}

export default Viewmap;
