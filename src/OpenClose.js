import React, { Component } from 'react';

class OpenClose extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
    this.handleOpenClose = this.handleOpenClose.bind(this);
  }

  handleOpenClose(d){

    var parts = d.split( /:|\s/ ),
    date  = new Date();
    date.setHours( + parts.shift() );
    date.setMinutes( + parts.shift() );
    return date;
  }

  render() {

    var open;

    if( this.props.start ){

      var today = new Date();
      var s = this.handleOpenClose( this.props.start );
      var e = this.handleOpenClose( this.props.end );

      if( e.getHours() < s.getHours() )
          e.setHours( e.getHours() + 24 );

      open = today <= e && today >= s ? 'open' : 'closed';
    }

    return(
      <span id = {open} > {open} </span>
    );
  }
}

export default OpenClose;
