import React, { Component } from 'react';
import './Footer.css';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

class Footer extends Component {
  constructor() {

    super();

    // Initialize state in the constructor; this is the only place you
    // can set the state directly without using this.setState

  }

  render() {

    return (
      <footer>
          <div className="container">
              <div className="row">


                        <ul className="social-network social-circle">
                            <li><a href="#" className="icoRss" title="Rss"><i className="fa fa-rss"></i></a></li>
                            <li><a href="#" className="icoFacebook" title="Facebook"><i className="fa fa-facebook"></i></a></li>
                            <li><a href="#" className="icoTwitter" title="Twitter"><i className="fa fa-twitter"></i></a></li>
                            <li><a href="#" className="icoGoogle" title="Google +"><i className="fa fa-google-plus"></i></a></li>
                            <li><a href="#" className="icoLinkedin" title="Linkedin"><i className="fa fa-linkedin"></i></a></li>
                        </ul>

              </div>
              <div className="row">
                  <span className="copyright">Copyright &copy; HungerQuest 2016</span>
              </div>
          </div>
      </footer>
    );
  }
}

export default Footer;
