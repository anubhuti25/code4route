import React, { Component } from 'react';
import './Newr.css';
import jsonData from './jsonData';
import OpenClose from './OpenClose';
import './Deleter.css';
import { Link } from 'react-router'
class Newe extends Component {
  constructor() {
    super();
    this.state={resRet:[]}
      this.handleOpenClose=this.handleOpenClose.bind(this);
  }

  componentDidMount(){
    var that=this;
    fetch('http://localhost:9000/restaurants?new=1',
    {
      method: 'GET',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>response.json())
     .then((json) => this.setState({resRet : json}))
  }

  handleOpenClose(d){
      var parts = d.split(/:|\s/),
      date  = new Date();
      date.setHours(+parts.shift());
      date.setMinutes(+parts.shift());
      return date;
  }

  render() {
    var results=this.state.resRet;
    var taskList1=results.map(function(j,i){
      var rObj
      var rName;
      var rLoc;
      var rArea;
      // rObj= j.image;
      rName=j.name;
      rLoc=j.address;
      rArea=j.area;
      //open
      var today = new Date();
      if(j.openTime){
      var startDate = this.handleOpenClose(j.openTime);
      var endDate   = this.handleOpenClose(j.closeTime);
      if(endDate.getHours()<startDate.getHours())
          endDate.setHours(endDate.getHours()+24);
      var open = today <= endDate && today >= startDate ? 'open' : 'closed';
    }

      return (
        <div className="col-md-4 col-sm-6 newr-item" key={i.toString()}>
            <Link to={"/Display/"+j.id} className="newr-link">
                <div className="newr-hover">
                    <div className="newr-hover-content">
                        <p className="text-muted">{rLoc}</p>
                    </div>
                </div>
                <div className = "newImg">
                  <img id="img" src={j.photosPath?j.photosPath[0]:''} className="" alt="" />
                </div>
            </Link>
            <div className="newr-caption">
                <h4>{rName}</h4>
                <div id="styl"> {rArea} - <OpenClose start = { j.openTime } end = { j.closeTime } /></div>
            </div>
        </div>
      );
    },this);

    if (results.length == 0){
      taskList1 = <h1 className = "centr" ><i id = "newSpin" className = "fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i></h1>
    }

    return (

      <section id="Newr" className="bg-light-gray">
          <div className="container">
              <div className="row">
                  <div className="col-lg-12 text-center">
                      <h2 className="section-heading">{this.props.heading}</h2>
                      <h3 className="section-subheading text-muted">{this.props.subheading}</h3>
                  </div>
              </div>
              <div className="row">
                  {taskList1}
              </div>
          </div>
      </section>
    );
  }
}

export default Newe;
