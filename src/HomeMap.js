import React, { Component } from 'react';
import './HomeMap.css';
import Nearby from './Nearby'
class HomeMap extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
     this.state={
       lat:'',
       lng:''
     }
  }

  componentDidMount(){
    var that = this;

    // var myLvar latLong = ipinfo.loc.split(",");atlng;
    //current location
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        that.setState({lat:17.411828179914195,lng:78.39848885622246})
        sessionStorage.myLatLng = pos;
      }, function(failure) {
        // console.log(failure);
        that.setState({lat:'17.411828179914195',lng:'78.39848885622246'})
        var myLatlong = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );
        sessionStorage.myLatLng = myLatlong;
      //   fetch('http://ipinfo.io',
      //   {
      //     method: 'GET',
      //     headers:{
      //       "Content-Type":"application/json",
      //       "Accept":"application/json"
      //     }
      //  })
      //  .then(response => response.json())
      //  .then(function(json){
      //    var latlong = json.loc.split(",");
      //    console.log(latlong);
      //    that.setState({lat:parseFloat(latlong[0]),lng:parseFloat(latlong[1])})
      //  })

      // var loc = {};
      //
      // if(google.loader.ClientLocation) {
      //   loc.lat = google.loader.ClientLocation.latitude;
      //   loc.lng = google.loader.ClientLocation.longitude;
      //
      //   var latlng = new google.maps.LatLng(loc.lat, loc.lng);
      //   // geocoder.geocode({'latLng': latlng}, function(results, status) {
      //   //     if(status == google.maps.GeocoderStatus.OK) {
      //   //         alert(results[0]['formatted_address']);
      //   //     };
      //   // });
      //   console.log(latlng.lat());
      //   that.setState({lat:latlng.lat(),lng:latlng.lng()})
    // }


        // window.location.reload
        // handleLocationError(true, infoWindow, map.getCenter());
      });
    }

    else {
      console.log("there");
      handleLocationError(false, infoWindow, map.getCenter());
    }
  }
  // function(position){console.log(position.message)},{timeout:10000, enableHighAccuracy: true});



  // navigator.geolocation.getCurrentPosition(function(position){console.log(position)},function(position){console.log(position.message)},{timeout:10000, enableHighAccuracy: true});

  render() {


    return (
      <div id="HomeMap">
      <h2 className = "centr"> NEARBY RESTAURANTS</h2>
      <h3 id="sysle" className="centr"> Find nearest restaurants from your Location </h3>
      <Nearby lat={this.state.lat} lng={this.state.lng}/>
      </div>
    );
  }
}

export default HomeMap;
