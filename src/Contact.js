import React, { Component } from 'react';
import "./App.css";
import "./Contact.css";

class Contact extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();

    // Initialize state in the constructor; this is the only place you
    // can set the state directly without using this.setState
    this.state={
      name:'',
      fetching:false,
      searching:false
    };
    this.handleContact=this.handleContact.bind(this);
    this.handleChange=this.handleChange.bind(this);
    this.handleValid=this.handleValid.bind(this);
  }

  handleChange(e){
    this.handleValid(e);
    if(e.currentTarget.id=="name1")
        this.setState({name:e.currentTarget.value})

  }

  handleValid(ele){

    var elements=Array.from(document.getElementsByClassName('mailing'));

    if(ele.currentTarget.value=="null"){
      document.getElementById("contactBtn").className = "btn btn-xl disabled";
      this.setState({fetching:false});
    }
    else {
      // console.log(/^[789][0-9]{9}$/.test(ele.currentTarget.value));
      if(ele.currentTarget.id=="phone"){
        if(!(/^[789][0-9]{9}$/.test(ele.currentTarget.value))){
          document.getElementById("contactBtn").className = "btn btn-xl disabled";
          this.setState({fetching:false});
          return;
        }
      }
      else if(ele.currentTarget.id=="eid"){
        if(!(/^[a-z0-9._]+@[a-z0-9.]+\.[a-z]{2,4}$/.test(ele.currentTarget.value))){
          document.getElementById("contactBtn").className = "btn btn-xl disabled";
          this.setState({fetching:false});
          return;
        }
      }
      document.getElementById("contactBtn").className = "btn btn-xl";
      this.setState({fetching:true});

    }

    elements.map(function(j,i){
      if(!j.value){
        document.getElementById("contactBtn").className = "btn btn-xl disabled";
        this.setState({fetching:false});
      }
    },this)
  }


  handleContact(){

    var n=this.state.name;
    // console.log(n);

    this.setState({searching:true});
    var form=JSON.stringify({
      name:n,
      phno:document.getElementById('phone').value,
      eid:document.getElementById('eid').value,
      message:document.getElementById('message').value
    });

    var that=this;

    if(this.state.fetching){

      document.getElementById("contactBtn").className = "btn btn-xl disabled";

      fetch('http://localhost:9000/mail',
      {
        method: 'POST',
        headers:{
          "Content-Type":"application/json",
          "Accept":"application/json"
        },
        body:form
       })
       .then(response=>console.log(response.status))
       .then(function(e){
         document.getElementById("contactBtn").className = "btn btn-xl disabled";
         document.getElementById("name1").value = "";
         document.getElementById("eid").value = "";
         document.getElementById("phone").value = "";
         document.getElementById("message").value = "";
         that.setState({searching:false});
       })
    }

  }

  render() {

    // console.log(this.state.fetching);

    return (
      <section id="contact">
          <div className="container">
              <div className="row">
                  <div className="col-lg-12 text-center">
                      <h2 className="section-heading">Contact Us</h2>
                      <h3 className="section-subheading text-muted"></h3>
                  </div>
              </div>
              <div className="row">
                      <div className="col-lg-12">
                        <form>
                          <div className="row">
                              <div className="col-md-6">
                                  <div className="form-group">
                                      <input type="text" className="form-control mailing"  onChange={this.handleChange} placeholder="Your Name *" id="name1" required data-validation-required-message="Please enter your name." />
                                      <p className="help-block text-danger"></p>
                                  </div>

                                  <div className="form-group">
                                      <input type="email" className="form-control mailing" placeholder="Your Email *" id="eid" onChange={this.handleValid} pattern = "[a-z0-9._]+@[a-z0-9.]+\.[a-z]{2,4}$" required data-validation-required-message="Please enter your email address." />
                                      <p className="help-block text-danger"></p>
                                  </div>
                                  <div className="form-group">
                                      <input type="text" className="form-control mailing" placeholder="Your Phone *" id="phone" onChange={this.handleValid} pattern="[789][0-9]{9}" required data-validation-required-message="Please enter your phone number." />
                                      <p className="help-block text-danger"></p>
                                  </div>
                              </div>
                              <div className="col-md-6">
                                  <div className="form-group">
                                      <textarea className="form-control mailing" placeholder="Your Message *" onChange={this.handleValid} id="message" required data-validation-required-message="Please enter a message."></textarea>
                                      <p className="help-block text-danger"></p>
                                  </div>
                              </div>
                              <div className="form-group">
                                  <center><a type="button" className="btn btn-xl disabled" id="contactBtn" onClick={this.handleContact}>Send Message {(this.state.searching)?<i className="fa fa-spinner fa-spin" aria-hidden="true"></i>:''}</a></center>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </section>
    );
  }
}

export default Contact;
