import React, { Component } from 'react';
import './App.css';
import Login from './Login.js';
import Contact from './Contact.js';
import Footer from './Footer.js';
import Navigate from './Navigate.js';
import Header from "./Header.js";
import HomeMap from "./HomeMap.js";
import Newr from "./Newe.js";
import 'bootstrap/dist/css/bootstrap.css';
import ReactDOM from 'react-dom';
import headTags from 'head-tags';

class App extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
  }
  
  componentDidMount(){
  }

  render() {
	  const options = {
		identifyAttribute: 'head-manager'
	  }
	  const headTitle = headTags({
		titleTemplate() {
		return 'HungerQuest - Explore your Cravings Restaurant Finder'
	  },
	  meta: [
		{charset: 'utf-8'}
	  ]
	  }, options) 
 
	  headTitle.title.toString() // <title>foo - My Website</title> 
	  headTitle.title.mount() // document.title changed!
	  
    return (

      <div className="App">

        <Navigate />

        <Header />
        <Newr heading="New" subheading="Want to try these" />
        <HomeMap />
        <Contact />
        <Footer />

      </div>

    );
  }
}

export default App;
