import React, { Component } from 'react';
import Rater from 'react-rater';
var map;
var bounds = new google.maps.LatLngBounds();
class Nearby extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
     this.state={
       latLong:[],
     }

     this.handleOpenClose=this.handleOpenClose.bind(this);
   }

   handleOpenClose(d){
       var parts = d.split(/:|\s/),
       date  = new Date();
       date.setHours(+parts.shift());
       date.setMinutes(+parts.shift());
       return date;
   }


  componentDidMount(){
    //create map
    var myLatlng = new google.maps.LatLng(17.3850,78.4867);
    map=new google.maps.Map(this.refs.map, {
      center:myLatlng,
      zoom: 12,
      scrollwheel: false
    });
  }

  componentWillReceiveProps(nextProps){
      if(nextProps.lat&&nextProps.lng){
        fetch('http://localhost:9000/restaurants?lat='+nextProps.lat+'&lng='+nextProps.lng,
        {
          method: 'GET',
          headers:{
            "Content-Type":"application/json",
            "Accept":"application/json"
          }
      })
      .then(response=>response.json())
      .then((json) => this.setState({latLong : json}))
      //my location marker and info window
      var myloc = new google.maps.LatLng(nextProps.lat,nextProps.lng);
      var myMarker=new google.maps.Marker({map: map,center:myloc,position:myloc});
      myMarker.setAnimation(google.maps.Animation.DROP);
      myMarker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
      var me= new google.maps.InfoWindow();
      me.setContent('<div class="myFont">My Location</div>');
      myMarker.addListener('mouseover',function(){
        me.open(map,myMarker)
      });
      bounds.extend(myloc);
    }
  }

  render() {

    var rlatLong = this.state.latLong;
    var that=this;
    var infowindw= new google.maps.InfoWindow();

    if(rlatLong){
      rlatLong.map(function(j,i){

        var myLatlong = new google.maps.LatLng(j.latitude,j.longitude);
        map.setCenter(myLatlong);

        var marker = new google.maps.Marker({
            position: myLatlong,
            map: map
        });
        marker.setAnimation(google.maps.Animation.DROP);
        bounds.extend(myLatlong);
        map.fitBounds(bounds);

        //open close timing check
        var today = new Date();
        var startDate;
        var endDate;
        {startDate = this.handleOpenClose(j.openTime)};
        {endDate   = this.handleOpenClose(j.closeTime)};

        if(endDate.getHours()<startDate.getHours())
          endDate.setHours(endDate.getHours()+24);

        var open = today <= endDate && today >= startDate ? 'open' : 'closed';

        //restaurant photo
        var photo= j.photosPath?JSON.stringify(j.photosPath[0]):'';
        //mouseover event for restaurant location marker
        marker.addListener('mouseover',function(){
          //current location of user
          var myLatlng = new google.maps.LatLng(that.props.lat,that.props.lng);

          //directions for route
          var directionsService = new google.maps.DirectionsService;
          var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers:true });

          var dist;

          directionsDisplay.setMap(map);
          //route determination
          directionsService.route({
              origin: myLatlng,
              destination: myLatlong,
              travelMode: 'DRIVING'
          }, function(response, status) {
              if (status === 'OK') {
                  dist = response.routes[0].legs[0].distance.text;
                  var time = response.routes[0].legs[0].duration.text;
                  //setting info window content
                  var contentString = '<div id="iw-container" >'+
                      '<img id="listImg" src='+photo+' />'+
                      '<div class="iw-content">'+
                      '<div class="iw-title">'+
                      '<a href="http://localhost:8081/#/Display/'+j.id+'">'+j.name+'</a>'+
                      '</div>'+
                      '<div>'+j.area +'&nbsp;<span id="'+open+'">'+open+'</span>'+'</div>'+
                      '<div>'+'<span class="boldin">Distance: </span>'+dist+'</div>'+
                      '<div>'+'<span class="boldin">Duration: </span>'+time+'&nbsp;<i class="fa fa-car"></i></div>'+
                      '</div>'+
                      '</div>';
                  infowindw.setContent(contentString);
                  infowindw.open(map,marker);
                  //adding directions to map
                  directionsDisplay.setDirections(response);

              } else {
                  window.alert('Directions request failed due to ' + status);
              }
          });
          //remove directions on closing the infowindow
          google.maps.event.addListener(infowindw,'closeclick',function(){
              map.fitBounds( bounds );
              directionsDisplay.setMap(null);
          });
        });
      },this);
    }

    return (
      <div >
        <div ref="map" style={{height:500}}>
          <infowindw>heloooooooooo</infowindw>
        </div>
      </div>
    );
  }
}

export default Nearby;
