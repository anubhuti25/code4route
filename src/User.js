import React, { Component } from 'react';
import Navbar from './Navbar';
import './Owner.css';
import Lists from './Lists';
import ChangePass from './ChangePass';
import { Router , Link , hashHistory } from 'react-router';
import headTags from 'head-tags';

class User extends Component {
  constructor() {
    super();
    this.state = {
      resRet : [],
      auth_token : localStorage.getItem( "auth_token" ),
      fetched : false
    }
    this.getFavRestaurants = this.getFavRestaurants.bind( this );
    this.signout = this.signout.bind( this );
  }

  changePass(e){

    document.getElementById('chgPassModal').style.display = "block";
  }

  signout(ll){

    var auth = "Bearer " + this.state.auth_token;
    var that = this
    var signout1 = localStorage.role == "owner" ? "ownerSignOut" : "userSignOut";
    localStorage.auth_token = '';
    localStorage.role = "";
    localStorage.uname = "";
    localStorage.oid = "";

    fetch( 'http://localhost:9000/' + signout1,
    {
      method : 'GET',
      headers :{
        "Authorization" : auth,
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
     })
     .then( response => console.log(response.status));
  }

  getFavRestaurants( e ){

    var that = this;
    var auth = "Bearer " + this.state.auth_token;

    fetch( 'http://localhost:9000/favourites ',
    {
      method : 'GET',
      headers :{
        "Authorization" : auth,
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
     })
     .then( function( response ){
       if( response.status != 200 ){
         localStorage.auth_token = '';
         localStorage.role = "";
         localStorage.uname = "";
         window.alert( "Time period expired. Please login again" );
         window.location.reload();
       }
       else {
         return response.json();
       }
     })
     .then(( json ) => that.setState({ resRet : json , fetched : true }));
  }

  componentDidMount(){
    this.getFavRestaurants();
  }

  render() {
    var resList=this.state.resRet;
    var rests;

    if ( resList.length == 0 && this.state.fetched ) {

      rests =
        <div className = "NoRes">
          <div id = "NoResText" >
            <i className="fa fa-frown-o" aria-hidden="true"></i> Sorry you have no favourites
          </div>

          <hr id = "br1" />

          <div className = "clearfix">
            <Link id = "AllResBtn" to= "/Listmapview/1/1" >
                See All Restaurants >
            </Link>
          </div>
        </div>
    }
    else {

      rests = <Lists restList = { this.state.resRet } page = { "user" }/>
    }
    //Adding a title in title bar
	  const options = {
		    identifyAttribute : 'head-manager'
	  }
	  const headTitle = headTags({
		    titleTemplate() {
		        return 'User Profile'
	      },
	      meta : [
		        {charset: 'utf-8'}
	      ]
	  }, options)

	  headTitle.title.toString()
	  headTitle.title.mount()

    var resList = this.props.children ? "listHidden" : "listVisible"
    if( localStorage.auth_token ){
      return (
        <div id="uscroll">
          <Navbar logout = {this.signout}/>

            <div id="page-content-wrapper">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12 bgPad">
                    <ChangePass />
                    <h1 className = "favHead centr" > My Favourites </h1>
                    <div id={resList}>
                      {rests}
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
      );
    }

    else {
      return(
        <div>{hashHistory.push('/')}</div>
      );
    }
  }
}

export default User;
