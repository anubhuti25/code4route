import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './Navbar.css';
import 'bootstrap/dist/css/bootstrap.css';
import {Link} from 'react-router';
var ll;
class Navbar extends Component {
  constructor() {
    super();
    this.state = {
      collapsed: true,
    };
    this.handleLogout=this.handleLogout.bind(this);

  }
  toggleCollapse() {
      const c= !this.state.collapsed;
      this.setState({collapsed:c});
  }
  handleLogout(){

    ll=localStorage.auth_token;
    localStorage.auth_token='';
    this.props.logout(ll);
  }

  changePass(e){

    document.getElementById('chgPassModal').style.display = "block";
  }

  componentDidMount(){

    window.addEventListener('storage', function(e) {
      window.location.reload();
    });
  }


  render() {

    const { collapsed } = this.state;
    const navClass = collapsed ? "collapse" : "";

    var prof2;
    if( localStorage.auth_token && navClass != "collapse" ){
      prof2 =<span>

              </span>

    }
    return (


      <nav id="navy" ref="navbar" className="navbar navbar-default navbar-custom navbar-fixed-top ">
          <div className="container">

              <div className="navbar-header ">
                  <button type="button" style = {{marginRight : "15px"}} className="navbar-toggle" onClick={this.toggleCollapse.bind(this)} data-target="#bs-example-navbar-collapse-1">
                      <span className="sr-only">Toggle navigation</span> <i className="fa fa-bars"></i>
                  </button>
                  <a className="navbar-brand page-scroll" href="#">HungerQuest</a>
              </div>

              <div className={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                <ul className="nav navbar-nav navbar-right">
                  <li className="hidden">
                      <a href="#"></a>
                  </li>
                  <li id="logout" className = "hidden-xs">
                      {localStorage.uname} <i className="fa fa-user fa-2x" aria-hidden="true"></i>
                      <ul className="dropdown2" id="dd3">
                          <li className = "link2" onClick = {this.handleLogout} ><Link to="/">Logout</Link></li>
                          <li className = "link2"> <Link  onClick = { this.changePass }>Change Password</Link>Logout</li>
                      </ul>
                  </li>
                </ul>

                <ul className="nav navbar-nav navbar-right visible-xs">
                  <li className="hidden">
                    <a href="#"></a>
                  </li>
                  {localStorage.auth_token?<li><Link className = "page-scroll" onClick = {this.handleLogout} to = "/" >Logout</Link></li>:''}
                  {localStorage.auth_token?<li><Link className = "page-scroll" onClick = {this.changePass} >Change Password</Link></li>:''}
                  <li>
                    <Link to="/Owner/Add" >Add Restaurant</Link>
                  </li>

                  <li>
                    <Link to="/Owner" >My Restaurants</Link>
                  </li>

                  </ul>
              </div>
          </div>
      </nav>
    );
  }
}

export default Navbar;
