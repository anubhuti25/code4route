import React, { Component } from 'react';
import './Lists.css';
import OpenClose from './OpenClose';
import Viewmap from './Viewmap';
import Search from './Search';
import AmPm from './AmPm';
import { Link } from 'react-router';
import Rater from 'react-rater';

class Lists extends Component {
  constructor() {
    super();
    this.resDet = this.resDet.bind(this);
  }

  resDet(e){
    var ide = e.currentTarget.id;
    ide = ide.substr(3)
    if(this.props.restList){
      var latlng2 = new google.maps.LatLng( this.props.restList[ide].latitude , this.props.restList[ide].longitude );
      this.props.resHovered( latlng2 )
    }
  }

  render() {
    var results=this.props.restList;
    var go=this.props.page;
    go=(go=="owner")?"/Owner/Delete/":"/Display/";



    var taskList1=results.map(function(j,i){

      var update = (this.props.page == "owner") ? <Link to={"/Owner/Delete/"+j.id} ><button id="upBtn" className="btn btn-danger">Update/Delete</button></Link>:"";

      var count=j.cuisine.length;
      var cuisine1=j.cuisine.map(function(c,k){
        if(k==(count-1))
        return(<span key={k}> {c.cuisine}</span>)
        return(<span key={k}> {c.cuisine},</span>)
      });

      return (
        <div className="well" key={i} onMouseEnter = {this.resDet} id = {"res"+i} >
          <div className="media">
            <Link className="pull-left" to={go+ j.id }>
              <div className="media-object">
              <img id="img1" src={j.photosPath?j.photosPath[0]:''} />
              </div>
        		</Link>

            <div className="media-body container">
            <div className="resName">
          	   <Link style = {{float : "left"}} to={go+ j.id } ><h4 className="media-heading">{j.name}</h4></Link>&nbsp;&nbsp;{update}
            </div>
               <div className="ta-right clearfix"><Rater id="sisin" rating={j.avg_rating}  interactive={false}/><button id="rating" className="btn btn-xs">{j.avg_rating?j.avg_rating:"NR"}</button></div>
               <div className="media-subheading" id="subhead">{j.area} <OpenClose start = { j.openTime } end = { j.closeTime } /></div>

               <ul className="list-inline list-unstyled">
                <li className="head">
                  <span>Cuisine: </span>
                </li>
                <li>
                  <span>{cuisine1} </span>
                </li>
               </ul>

               <ul className="list-inline list-unstyled">
                <li className="head">
                  <span>Address: </span>
                </li>
                <li>
                  <span>{j.address} </span>
                </li>
               </ul>
               <hr />
               <div className="row">
  			        <div className="col-md-6 col-lg-4 col-sm-12"><span><i className="glyphicon glyphicon-time"></i>&nbsp;</span><span className="smallFont"><AmPm e = {j.openTime} /> - <AmPm e = {j.closeTime} />  </span></div>
                <div className="col-md-6 col-lg-4 col-sm-12"><i className="fa fa-rupee"></i> <span className="smallFont">{j.cost}</span><span id="approx2">(Approx 2)</span></div>
                <div className="col-md-6 col-lg-4 col-sm-12"><span className="smallFont"><a href="tel:{j.phn}"><i id="phnn" className="fa fa-phone"/>{j.phn}</a></span>
                <span><a href={j.fbUrl} target="_blank"><i id = "facb" className="fa fa-facebook-square fa-3x"></i></a></span>
                </div>
			         </div>
            </div>
          </div>
        </div>
      );
    },this);

    return (
      <div>{taskList1}</div>
    );
  }
}

export default Lists;
