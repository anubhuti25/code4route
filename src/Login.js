import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "./Login.css";
import Signup from "./Signup.js";
import Owner from "./Owner.js";
import { Link, hashHistory } from 'react-router';

var li;

class Login extends Component {
  constructor() {
    super();
    this.state={
      role:'',
      Owner:[],
      Res:[],
      UserName:'',
      Password:'',
      forgotPass:'',
      forgetP:false,
      fetching:false

    };

    this.handleRegister = this.handleRegister.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleAuth = this.handleAuth.bind(this);
    this.hc = this.hc.bind(this);
    this.sc = this.sc.bind(this);
    this.handleUserName = this.handleUserName.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.handleReg = this.handleReg.bind(this);
    this.handleForget = this.handleForget.bind(this);
    this.forgetClick = this.forgetClick.bind(this);
    this.goBack = this.goBack.bind(this);
    this.handleFor = this.handleFor.bind(this);

  }

  checkAllEntered(){

    var elements1 = Array.from( document.getElementById( 'logForm' ).getElementsByClassName( 'form-control' ));
    var flag = true;
    elements1.map( function(j , i){
      if( !j.value ){
        flag = false;
      }
    });

    if( !(document.getElementById( 'ownercheck1' ).checked || document.getElementById( 'usercheck1' ).checked)){
      flag = false;
    }

    if( flag ){
      document.getElementById( 'logBtn' ).className = "btn btn-primary btn-lg";
      this.setState({ fetching : true });
    }
  }

  handleUserName(e){
    this.checkAllEntered();
    this.setState({ UserName : e.target.value });
  }

  handlePassword(e){
    this.checkAllEntered();
    this.setState({ Password : e.target.value });
  }

  handleCheck(e){

    if( document.getElementById( 'ownercheck1' ).checked ){
      this.setState({ role : 'owner' });
      this.checkAllEntered();
       li = document.getElementById( "links" ).href = "http://localhost:8081/#/Owner/";
    }
    else if( document.getElementById('usercheck1').checked ){
      this.setState({ role : ''});
      this.checkAllEntered();
      li = document.getElementById( "links" ).href = "http://localhost:8081/#/User";
    }
    else {
      this.setState({fetching : false})
    }
  }

  hc(e){

    document.getElementById( 'loginModal' ).style.display = "block";
    if( !this.state.fetching )
        document.getElementById( 'logBtn' ).className = "btn btn-primary btn-lg disabled"
  }

  handleReg(e){
    document.getElementById( 'loginModal' ).style.display = "none";
    document.getElementById( 'myModal' ).style.display = "block";
  }

  sc(e){
    document.getElementById( 'loginModal' ).style.display = "none";
    if( !this.state.forgetP ){
      document.getElementById( 'ownercheck1' ).checked = false;
      document.getElementById( 'usercheck1' ).checked = false;
    }

    if( this.state.forgetP ){
      document.getElementById( 'securityQues' ).value = '';
      document.getElementById('name2').value = '';
      document.getElementById('answer').value = '';
    }

    this.setState({
      UserName : '',
      Password : '',
      role : '',
      forgetP : false,
      fetching : false
    });

    document.getElementById( 'errMsg' ).innerHTML= '' ;

  }

  handleRegister(e){
      //validation
    e = document.getElementById( "links" );
    var that = this;
    var tname = ( this.state.role == 'owner' ) ? "oname" : "uname";
      // password
    var hname = document.getElementById( 'name1' ).value;
    var password = document.getElementById( 'password1' ).value;
    var nowRole = ( this.state.role == 'owner' ) ? 'ownerSignIn' : 'userSignIn';

    // console.log(nowRole);
    var form;
    if( tname == "oname" ){

      form = JSON.stringify({
          oname : hname,
          pwd : password
      });
    }
    else {

      form = JSON.stringify({
          uname : hname,
          pwd : password
      });
    }
      //calling Authorization
    fetch( 'http://localhost:9000/' + nowRole,
    {
      method: 'POST',
      headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
      },
      body : form
    })
    .then( function ( response ){

      if( response.status == 200 ){
          localStorage.role = that.state.role;
          return response.json();
      }
      else {
          return response.text();
      }
    })
    .then( function ( results ){

      if( results == "Invalid credentials" ){

          document.getElementById( 'errMsg' ).innerHTML = "Invalid Username and Password";
          that.setState({Owner : undefined})
      }
      else if ( results == "Your email id is not verified." ) {
          document.getElementById( 'errMsg' ).innerHTML = results;
          that.setState({Owner : undefined})
      }
      else{
          ( that.state.role == "owner" ) ? localStorage.uname = results.oname : localStorage.uname = results.uname;
          that.setState({ Owner : results })
      }
    })
    .then( function( response ){
      if( that.state.Owner ){
          that.handleAuth( that.state.Owner );
          if( that.state.role == "" ){
            if( that.props.lists )
              that.props.closeMod1();
            else {
              that.props.closeMod();
            }
          }
          if( hashHistory.getCurrentLocation().pathname == '/' )
            window.location.replace( li );
          else {
            window.location.reload();
          }
      }
    });
  }

  handleFor(e){

    var tname = document.getElementById( 'name2' ).value;
    var ques = document.getElementById('securityQues').value;
    var ans = document.getElementById('answer').value;
    var nowRole = ( this.state.role == "owner" ) ? "temPwd" : "temPwdUser";
    var form;
    e.currentTarget.className = "btn btn-primary btn-lg disabled";
    document.getElementById( 'logSpinning1' ).className = "fa fa-spinner fa-spin";

    if( this.state.role == "owner" ){
      form  = JSON.stringify({
        oname : tname,
        secQuestion : ques,
        secAns : ans
      });
    }
    else {
      form  = JSON.stringify({
        uname : tname,
        secQuestion : ques,
        secAns : ans
      });
    }

    var that = this;
    fetch( 'http://localhost:9000/' + nowRole,
    {
      method: 'POST',
      headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
      },
      body : form
    })
    .then( response => response.text() )
    .then( function( results ){
      if( results == "" ){
        window.alert( "A mail has been to your registered email with new Password which will be valid till the next one hour.Please login with temporary Password and then reset it" );
        that.setState({ forgetP : false })
      }
      else {
        document.getElementById( 'errMsg' ).innerHTML = results;
        document.getElementById( 'done' ).className = "btn btn-primary btn-lg";
      }
    })
  }

  handleAuth(e){

    localStorage.auth_token = this.state.Owner.auth_token;
    localStorage.role = this.state.role == "owner"? "owner" : "user";
  }

  forgetClick(){

    var tname = document.getElementById( 'name1' ).value;
    // console.log(tname);
    var nowRole1 = ( this.state.role == "owner" ) ? "forgotPwd" : "forgotPwdUser";

    var that = this;

    var form;
    if( this.state.role == "owner" ){
      form  = JSON.stringify({
        oname : tname
      });
    }
    else {
      form  = JSON.stringify({
        uname : tname
      });
    }

    fetch( 'http://localhost:9000/' + nowRole1,
    {
      method : 'POST',
      headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
      },
      body : form
    })
    .then( function( response ){
      if( response.status == 200 )
          return response.json();
      else if ( response.status == 400 ) {
        return response.text();
      }
    })
    .then( function( results ){
      if( results == "username does not exist" ){
        // console.log( "in err" );
        document.getElementById( 'errMsg' ).innerHTML = results;
      }
      else {
        that.setState({ forgotPass : results , forgetP:true });
      }
    })
    .then( function( e ){
      if( that.state.forgotPass ){
        var x = that.state.forgotPass;
        document.getElementById( 'securityQues' ).value = x.secQuestion;
        document.getElementById( 'name2' ).value = ( that.state.role == "owner" ? x.oname : x.uname );
        document.getElementById( 'securityQues' ).setAttribute( "disabled" , true );
        document.getElementById( 'name2' ).setAttribute( "disabled" , true );
      }
    });
  }

  goBack(){
    this.setState({ forgetP:false })
  }

  handleForget(e){
    document.getElementById( 'forgotModal' ).style.display = "block"
  }

  render(){

    if( !this.state.forgetP )
    return(
      <div className = "Login">
        <button type = "button" id = "myBtn" className = "btn btn-info smaller" onClick = { this.hc }> Login </button>
        <div id = "loginModal" ref = "loginModal" className = "modal">
          <div className = "modal-dialog">
            <div className = "modal-content">
              <div className = "modal-header">
                <button type = "button" className = "close" id = "loginClose" onClick = { this.sc }> &times; </button>
                <h2 className = "modal-title"> Login </h2>
                <span id = "logHead"> Enter username and password to login </span>
              </div>

              <div className = "modal-body">
                <form className = "form" id = "logForm" role = "form" >
                  <div id = "errMsg"></div>

                  <div className = "form-group">
                    <label className = "sr-only" htmlFor = "exampleInputEmail2"> UserName </label>
                    <input type = "text" value = { this.state.UserName } onChange = { this.handleUserName }
                     className = "form-control" id = "name1" placeholder = "UserName" required />
                  </div>

                  <div className = "form-group">
                    <label className = "sr-only" htmlFor = "exampleInputPassword2"> Password </label>
                      <input type = "password" minLength = "8" value = { this.state.Password } onChange = { this.handlePassword }
                      className = "form-control" id = "password1" placeholder = "Password" required />
                      <span id = "require"></span>
                      <span >
                          <Link id="forgotPwd" onClick={this.forgetClick}>Forgot Password?</Link>
                      </span>
                  </div>

                  <div className = "form-group">
                    <label>
                      <input type = "radio" name = "check" value = "owner" id = 'ownercheck1' onChange = { this.handleCheck } />
                      Owner
                    </label>

                    <label>
                      <input type = "radio" name = "check" value = "customer" id = 'usercheck1' onChange = { this.handleCheck } />
                      Customer
                    </label>
                  </div>

                  <div className = "form-group">
                    <Link to = { hashHistory.getCurrentLocation().pathname } id = "links">
                      <center> <button onClick = { this.handleRegister } id = "logBtn" className = "btn btn-primary btn-lg"> Log in </button></center>
                    </Link>
                  </div>

                  <div>
                    <center>

                      <span id = "register"> If you are not registered? Click here:
                        <li>
                          <Link to = { hashHistory.getCurrentLocation().pathname }  id = "but" onClick = { this.handleReg }> Signup </Link>
                        </li>
                      </span>
                    </center>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

    else {
      return(

        <div className = "Login">
          <button type = "button" id = "myBtn" className = "btn btn-info" onClick = { this.hc }> Login </button>
          <div id = "loginModal" ref = "loginModal" className = "modal">
            <div className = "modal-dialog">
              <div className = "modal-content">
                <div className = "modal-header">
                  <button type = "button" className = "close" id = "loginClose" onClick = { this.sc }> &times; </button>
                  <h2 className = "modal-title"> Forgot password ? </h2>
                </div>

                <div className = "modal-body">

                    <div id = "errMsg"></div>

                    <div className = "form-group">
                      <label className = "sr-only"> UserName </label>
                      <input type = "text" className = "form-control" id = "name2" placeholder = "UserName" required />
                    </div>

                    <div className = "form-group">
                      <span id="secQues">Security Question:</span>
                      <label className = "sr-only"> Security Question </label>
                      <input type = "text" className = "form-control" id = "securityQues" placeholder = "Security Question" required />
                    </div>

                    <div className = "form-group">
                      <label className = "sr-only"> Answer </label>
                      <input type = "text" className = "form-control" id = "answer" placeholder = "Answer" required />
                    </div>

                    <div className = "form-group">
                        <center>
                          <button className = "btn btn-primary btn-lg" onClick = { this.goBack }> Go Back </button>
                          <button className = "btn btn-primary btn-lg" id="done" onClick={ this.handleFor }> Done  <i id="logSpinning1" aria-hidden="true"></i></button>
                        </center>
                    </div>
                </div>

              </div>
            </div>
          </div>
        </div>

      );
    }
  }
}

export default Login;
