import React, { Component } from 'react';
import Navbar from './Navbar';
import './Owner.css';
import Newe from './Newe';
import Lists from './Lists';
import './ChangePass.css';
import { Router,Link,hashHistory } from 'react-router';

class ChangePass extends Component {
  constructor() {
    super();
    this.state={
      auth_token : localStorage.getItem("auth_token")
    }
    this.handlePop = this.handlePop.bind(this);
    this.handlePass = this.handlePass.bind(this);
    this.checkAll = this.checkAll.bind(this);
    this.handleRePass = this.handleRePass.bind(this);
  }

  checkAll(e){

    var flag = true;
    Array.from( document.getElementById( 'changeForm' ).getElementsByClassName( 'form-control' )).map( function( j , i ){
      if( !j.value ){
        flag = false;

      }
    } , this);

    if( flag ){
      document.getElementById( 'chgBtn' ).className = "btn btn-primary btn-lg";
    }

    else {
      document.getElementById( 'chgBtn' ).className = "btn btn-primary btn-lg disabled";
    }
  }

  handlePass(e){

    if(( e.target.value.match( "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,}$" ) != null)){
      this.checkAll();
      document.getElementById( 'pwdPopup' ).style.visibility = "hidden"
      // this.setState({Password : e.target.value});
    }

    else{

      var ele = document.getElementById( 'pwdPopup' );
      ele.style.visibility= "visible";
      this.handlePop(ele);
      // this.setState({fetching : false})
    }
  }

  handleRePass(e){

    if(e.target.value == document.getElementById( 'newPass' ).value){
      this.checkAll();
      document.getElementById( "passwordError" ).innerHTML = "";
      // this.setState({Pass : e.target.value});

    }
    else {

      document.getElementById( "passwordError" ).innerHTML = "Passwords do not match";
      // this.setState({fetching : false})
    }
  }

  handlePop(a){

    a.style.display = "block"
    a.classList.toggle( "show" );
  }

  chgPass(e){

   // document.getElementById('logSpinning').className = "fa fa-spinner fa-spin";
    document.getElementById( 'chgBtn' ).className = "btn btn-primary btn-lg disabled";
    var nowRole = ( localStorage.role == "owner" ) ? "owners" : "users";

    var form = JSON.stringify({
        oldPwd : document.getElementById( 'oldPass' ).value,
        newPwd : document.getElementById( 'newPass' ).value
    });

    var auth = "Bearer " + localStorage.auth_token;

    fetch('http://localhost:9000/' + nowRole,
    {
      method: 'PUT',
      headers:{
        "Authorization":auth,
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
      body : form
     })
     .then(function(response){
		 if(response.status == 200){
			console.log("ok");
			window.location.reload();
		 }
		 else if(response.status == 400){
			 console.log("wrong pwd")
			document.getElementById('valPwd').innerHTML = "Your old password did not match";
		 }
	 });
	 
  }

  sc(e){
    document.getElementById('chgPassModal').style.display = "none";
  }

  render() {

    return (
      <div id = "chgPassModal" className = "modal">
        <div className = "modal-dialog">
          <div className = "modal-content">
            <div className = "modal-header">
              <button type = "button" className = "close" id = "closeBtn" onClick = { this.sc } > &times; </button>
              <h2 className = "modal-title"> Change Password </h2>
            </div>

            <div className = "modal-body">
              <form id = "changeForm" className = "form" role = "form" acceptCharset = "UTF-8">
                <div id = "oldPass1" className = "form-group">
                  <label className = "sr-only" htmlFor="exampleInputName"> Old Password </label>
                  <input type = "password" className = "form-control" id = "oldPass" placeholder = "Old/Temporary Password" onChange={this.checkAll} required  />
                </div>
				<p id="valPwd"></p>

                <div id = "newPass1" className = "form-group">
                  <div className="popup">
                    <label className = "sr-only" htmlFor="exampleInputName"> New Password </label>
                    <input type = "password" className = "form-control" id = "newPass" placeholder = "New Password" onChange={this.handlePass} required  />
                    <span className="popuptext" id="pwdPopup">Password must contain atleast 8 characters, one uppercase ,one lowercase and one number</span>
                  </div>
                </div>

                <div id = "rePass1" className = "form-group">
                  <label className = "sr-only" htmlFor="exampleInputName"> Old Re-enter Password </label>
                  <input type = "password" className = "form-control" id = "rePass" placeholder = "Re-enter Password" onChange={this.handleRePass} required  />
                  <span id = "passwordError"></span>
                </div>


                <div className="form-group">
                    <center>
                        <Link type="button" className="btn btn-primary btn-lg disabled" id="chgBtn" onClick={this.chgPass} >
                            Change 
                        </Link>
                    </center>
                    <span id="Error"></span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  );
}

}

export default ChangePass;
