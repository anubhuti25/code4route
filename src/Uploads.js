import React, { Component } from 'react';
import ViewImage from './ViewImage';

class Uploads extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
    this.state={
    }
    this.deleteImg=this.deleteImg.bind(this);
  }

  deleteImg(i){

    var menus1;
    if(this.props.menu1){
      menus1="menu"
    }
    this.props.handleFiles(i.currentTarget.id,menus1);
  }

  render() {


    var images=this.props.images1;
    var len = images?images.length:0;

    var count = 1;
    if(len){
    var imagesList=images.map(function(j,i){
      if(j){
        return(
          <div key={i}>
            <a href={j} target="_blank">View Image {count++}</a> | <a id={i} onClick={this.deleteImg}>Delete</a>
          </div>
      );
      }

  },this);
}
    return (
      <div className="Uploads">
        {imagesList}
      </div>
    );
  }
}

export default Uploads;
