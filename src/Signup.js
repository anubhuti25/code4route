import React, { Component } from 'react';
import "./Signup.css";
import Owner from "./Owner.js";
import { Link , hashHistory } from 'react-router';

class Signup extends Component {
  constructor() {
    super();

    this.state = {
      role : '',
      UserName : '',
      Password : '',
      Pass : '',
      email : '',
      fetching : false
    }

    this.handleRegister1 = this.handleRegister1.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handleUserName = this.handleUserName.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handlePop = this.handlePop.bind(this);
    this.hc = this.hc.bind(this);
    this.sc = this.sc.bind(this);
    this.handleSecQues = this.handleSecQues.bind(this);
    this.handleAnswer = this.handleAnswer.bind(this);
  }

  checkAll(){

    var elements1 = Array.from(document.getElementById('signForm').getElementsByClassName('form-control'));
    var flag = true;
    elements1.map( function(j , i){
      if( !j.value ){
        flag = false;
      }
      else if (( j.id == "sel1" ) && ( j.selectedIndex == 0 )) {
        document.getElementById('secErr').style.display = "block"
        document.getElementById('secErr').innerHTML="Please choose Security Question"
        flag=false;
      }
    });

    if( !(document.getElementById( 'ownercheck' ).checked || document.getElementById( 'usercheck' ).checked)){
      flag=false;
    }

    if( flag ){
      document.getElementById( 'signBtn' ).className = "btn btn-primary btn-lg";
      this.setState({fetching : true});
    }
  }

  handleCheck(e){

    if( document.getElementById( 'ownercheck' ).checked ){
      this.setState({role : 'owner'});
      this.checkAll();
    }
    else if( document.getElementById('usercheck').checked ){
      this.setState({role : '' });
      this.checkAll();
    }
    else {
      this.setState({fetching : false})
    }
  }

  handleSecQues(e){
    if( e.currentTarget.selectedIndex != 0 ){
      document.getElementById('secErr').style.display = "none"
      this.checkAll();
    }
    else {
      document.getElementById('secErr').style.display = "block"
      document.getElementById('secErr').innerHTML = "Please choose a Security question"
      this.setState({ fetching : false})
    }
  }

  handleAnswer(e){

    if(e.currentTarget.value){
      this.checkAll();
    }
    else {
      this.setState( { fetching : false })
    }
  }

  handleUserName(e){

    if(e.target.value.match( "^[a-zA-Z0-9]+$" ) != null){
        this.checkAll();
        this.setState({UserName : e.target.value});
    }
    else {
      this.setState({fetching : false});
    }
  }

  handleEmail(e){
    if(( e.target.value.match( "^[a-z0-9._%+-]+@[a-z.-]+\.[a-z]{2,3}$" ) != null)){
      this.checkAll();
      document.getElementById( 'emailPopup' ).style.visibility = "hidden"
      // this.setState({Password : e.target.value});
    }

    else{

      var ele2 = document.getElementById( 'emailPopup' );
      ele2.style.visibility = "visible";
      this.handlePop( ele2 );
      this.setState({fetching : false})
    }
  }

  handlePassword(e){
    //console.log(e.target.value.match("^[a-zA-Z0-9!@#$%^&*~()_+]*$"));
    if(( e.target.value.match( "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9@.]{8,}$" ) != null)){
      this.checkAll();
      document.getElementById( 'myPopup' ).style.visibility = "hidden"
      this.setState({Password : e.target.value});
    }

    else{

      var ele = document.getElementById( 'myPopup' );
      ele.style.visibility= "visible";
      this.handlePop(ele);
      this.setState({fetching : false})
    }
  }

  handleConfirm(e){

    if(e.target.value == document.getElementById( 'password' ).value){
      this.checkAll();
      document.getElementById( "passwordError" ).innerHTML = "";
      this.setState({Pass : e.target.value});

    }
    else {

      document.getElementById( "passwordError" ).innerHTML = "Passwords do not match";
      this.setState({fetching : false})
    }
  }

  handlePop(a){

    a.style.display = "block"
    a.classList.toggle( "show" );
  }

  hc(e){

    document.getElementById( 'myModal' ).style.display = "block";
  }

  sc(e){

    document.getElementById( 'myModal' ).style.display = "none";
    this.setState({
      UserName : '',
      Password : '',
      Pass : '',
    });

    document.getElementById( 'myPopup' ).style.visibility = "hidden"
    document.getElementById( 'emailPopup' ).style.visibility = "hidden"
    document.getElementById( 'logSpinning' ).className= "" ;

    Array.from( document.getElementById( 'signForm' ).getElementsByClassName( 'form-control' )).map( function(j,i){
      j.value = '';
    });

    document.getElementById('ownercheck').checked = false;
    document.getElementById('usercheck').checked = false
    document.getElementById( 'Error' ).innerHTML= '' ;
    document.getElementById( 'passwordError' ).innerHTML= '' ;
  }

  handleRegister1(e){

    document.getElementById( 'logSpinning' ).className = "fa fa-spinner fa-spin"
    document.getElementById( 'signBtn' ).className = "btn btn-primary btn-lg disabled";

    var inputEle = Array.from( document.getElementById( 'signForm' ).getElementsByClassName( 'form-control' ));
    inputEle.map( function( j , i ){
        j.className = "form-control disabled"
    });

    if( this.state.role == 'owner' )
    {
      var form = JSON.stringify({
          oname : document.getElementById( 'Name' ).value,
          eid : document.getElementById( 'email' ).value,
          pwd : document.getElementById( 'password' ).value,
          secQuestion : document.getElementById( 'sel1' ).value,
          secAns : document.getElementById( 'answer1' ).value,
      });
    }
    else{
      var form = JSON.stringify({
          uname : document.getElementById( 'Name' ).value,
          eid : document.getElementById( 'email' ).value,
          pwd : document.getElementById( 'password' ).value,
          secQuestion : document.getElementById( 'sel1' ).value,
          secAns : document.getElementById( 'answer1' ).value,
        });
    }

    var that = this;
        // console.log(form);
    var nowRole = (this.state.role == 'owner' ) ? 'owners' : 'users';

          // console.log(nowRole);
          //calling POST for add Owner
    fetch( 'http://localhost:9000/' + nowRole + '' ,
    {
      method: 'POST',
      headers: {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
      },
      body : form
    })
    .then( function( response ){
      if( response.status == 200 )
          return response.json();
      else if( response.status == 400 ){
          return response.text();
        }
      })
      .then( function( results ){
        if( results == "Username already exists" ){
            document.getElementById( 'logSpinning' ).className = ""
            document.getElementById( 'signBtn' ).className = "btn btn-primary btn-lg";
            document.getElementById( 'Error' ).innerHTML = results;
        }

        else if ( results == "Email id already registered" ) {
            document.getElementById( 'logSpinning' ).className = ""
            document.getElementById( 'signBtn' ).className = "btn btn-primary btn-lg";
            document.getElementById( 'Error' ).innerHTML = results;
        }

        else {

            window.alert( "Successfully Signed up. A mail has been sent to your email address to verify your id. Please do verify email id to login" );
            that.setState({fetching:false})
            that.sc();
            // document.getElementById( 'loginModal' ).style.display = "block";
        }
      });
    }

    componentDidMount(){
      if( ! this.state.fetching){
          document.getElementById( 'signBtn' ).className = "btn btn-primary btn-lg disabled";
      }
    }


  render(){
    return(
      <div className = "Signup">
        <button type = "button" id = "myBtn" className = "btn btn-info smaller" onClick = { this.hc }> Signup </button>
        <div id = "myModal" className = "modal">
          <div className = "modal-dialog">
            <div className = "modal-content">
              <div className = "modal-header">
                <button type = "button" className = "close" id = "closeBtn" onClick = { this.sc }> &times; </button>
                <h2 className = "modal-title"> SIGN UP </h2>
                <span id = "logHead"> All the fields should be filled </span>
              </div>

              <div className = "modal-body">
                <form id = "signForm" className = "form" role = "form" acceptCharset = "UTF-8">
                  <span id="Error"></span>
                  <div id = "UserName" className = "form-group">
                    <label className = "sr-only" htmlFor="exampleInputName"> UserName </label>
                    <input type = "text" onChange = { this.handleUserName } className = "form-control" id = "Name" placeholder = "UserName" required  />
                  </div>

                  <div className = "form-group">
                    <label className="sr-only" htmlFor="exampleInputEmail2">Email Address </label>
                    <div className="popup" >
                      <input id="email" type="email" pattern="[a-z0-9._%+-]+@[a-z.-]+\.[a-z]{2,3}$" className="form-control"  onChange={this.handleEmail} placeholder="Email" required />
                      <span className="popuptext" id="emailPopup">Email Id should be of the form example@something.com</span>
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="sr-only" htmlFor="exampleInputPassword2">New Password</label>
                    <div className="popup" >
                      <input type="password" minLength = "8"
                            className="form-control " id="password" placeholder="Password" required onChange={this.handlePassword}/>
                      <span className="popuptext" id="myPopup">Password must contain atleast 8 characters, one uppercase ,one lowercase and one number</span>
                    </div>
                  </div>

                  <div className="form-group" id="Password">
                    <label className="sr-only" htmlFor="exampleInputPassword2">Retype Password</label>
                    <input type="password" minLength = "8"
                        className="form-control" id="Repassword"  placeholder="Retype Password" required  onChange={this.handleConfirm}/>
                    <span id="passwordError"></span>
                  </div>

                  <div className="form-group" id="Password">
                    <label className="sr-only" htmlFor="sel1">Select list:</label>
                    <select className="form-control" id="sel1" onChange = { this.handleSecQues } >
                      <option value="Security Question">Choose Security Question</option>
                      <option value="Favourite Color">Favourite Color</option>
                      <option value="Your Birthplace">Your Birthplace</option>
                      <option value="Your primary school name">Your primary school name</option>
                      <option value = "Your phone number">Your phone number</option>
                      <option value = "Your favourite movie">Your favourite movie</option>
                    </select>
                  </div>
                  <span id="secErr"></span>

                  <div className="form-group">
                    <label className="sr-only" htmlFor="answer">Answer</label>
                    <input type="password" minLength = "8"
                        className="form-control" id="answer1"  placeholder="Answer" onChange = {this.handleAnswer} />
                  </div>

                  <div className="form-group">
                    <label>
                      <input type="radio" name="check" value="owner" id='ownercheck' onChange={this.handleCheck} />
                      Owner
                    </label>

                    <label>
                      <input type="radio" name="check" value="customer" id="usercheck" onChange={this.handleCheck}/>
                      Customer
                    </label>
                  </div>

                  <div className="form-group">
                      <center>
                          <Link type="button" onClick={this.handleRegister1} className="btn btn-primary btn-lg" id="signBtn">
                              Sign up <i id="logSpinning" aria-hidden="true"></i>
                          </Link>
                      </center>
                      <span id="Error"></span>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
  }
}

export default Signup;
