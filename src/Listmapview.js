import React, { Component } from 'react';
import Lists from './Lists';
import Viewmap from './Viewmap';
import Snav from './Snav';
import Filters from './Filters';
import './Filters.css';
import './Listmapview.css'
import { Link } from 'react-router';

const BASE_URL = 'http://localhost:9000/restaurants';

class Listmapview extends Component {
  constructor() {
    super();

    this.state = {
      resRet : [],
      filters : [],
      change : false,
      cleared : false,
      sorting : [],
      latlng3 : '',
      fetched : false
    };

    this.search = this.search.bind(this);
    this.removeFilter = this.removeFilter.bind(this);
    this.formQuery = this.formQuery.bind(this);
    this.newRes = this.newRes.bind(this);
    this.getNew = this.getNew.bind(this);
    this.nearByRes = this.nearByRes.bind(this);
    this.hovered = this.hovered.bind(this);
  }

  hovered(a){
    this.setState({ latlng3 : a })
  }

  nearByRes(){

    var lat1 , lng1;
    var that = this;

      this.setState( { fetched : false , latlng3 : undefined } )

    if ( navigator.geolocation ) {
      navigator.geolocation.getCurrentPosition( function( position ) {
        var pos = {
          lat : position.coords.latitude,
          lng : position.coords.longitude
        };

        lat1 = position.coords.latitude;
        lng1 = position.coords.longitude;

        var myLatlong = new google.maps.LatLng( lat1 , lng1 );
        that.getNew( myLatlong )

      }, function() {
        var myLatlong = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );
        that.getNew(myLatlong)
        handleLocationError( true , infoWindow , map.getCenter() );
      });
    }
  }

  getNew( pos ){

    fetch( 'http://localhost:9000/restaurants?lat=' + pos.lat() + '&lng=' + pos.lng(),
    {
      method : 'GET',
      headers :{
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
    })
    .then( response => response.json() )
    .then(( json ) => this.setState({ resRet : json , filters : [] , cleared : true , fetched : true }))
  }

  newRes(){

    var that = this;

      this.setState( { fetched : false , latlng3 : undefined } )

    fetch( 'http://localhost:9000/restaurants?new=1',
    {
      method : 'GET',
      headers : {
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
    })
    .then( response => response.json() )
    .then(( json ) => this.setState({ resRet : json , filters : [] , cleared : true , fetched : true }))
  }

  search(e){

    this.setState( { fetched : false , latlng3 : undefined } )

    var filts = [];
    var sortFunction;
    var fetching = true;
    filts = this.state.filters;
    var length = this.state.filters.length;
    var q = '';

    var index = filts.findIndex(function(element){
      return( element.rests == e.rests );
    });

    if( index != -1 )
      filts.splice( index , 1);

    if(( e.rests != "sort" )){

      q = this.formQuery(filts);

      if(e.rests == "cost")
        q += 'high=' + e.value[1] + '&&low=' + e.value[0];

      else {
            q += e.rests + '=' + e.value;
      }
    }
    else{

      fetching = false;
      var sortFunction = this.sortResults(e);
      var results = this.state.resRet;
      results = results.sort(sortFunction);

      var obj = new Object({
        rests : e.rests,
        value : e.value
      });

      filts.unshift(obj);
      this.setState({ resRet : results , filters : filts , change : false , cleared : false , fetched : true });
    }

    var that = this;

    if( fetching ){
      const url = BASE_URL+'?'+q;
      console.log(url)
      fetch( url ,
      {
        method : 'GET',
        headers :{
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        }
     })
     .then( response => response.json() )
     .then(function(json){

       var obj = new Object({
         rests : e.rests,
         value : e.value
       });

      //  if( ( filts.length == 7 ) && ( !this.state.change )){
      //     filts.pop();
      //  }
       //
      //   else if (( filts.length == 8 )&&( this.state.change )) {
      //     filts.pop();
      //   }

      console.log(filts);
        filts.unshift(obj);
        that.setState({ resRet : json , filters : filts , cleared : false , fetched : true })

      })
      .then(function(){
        if( that.state.change ){
          var sortFunction = that.sortResults( that.state.sorting );
          var results = that.state.resRet;
          results = results.sort( sortFunction );
          that.setState({resRet : results })
        }
      });
    }
  }

  formQuery(f){

    var query = '';
    var sortVal = false;
    f.map(function( j , i ){

      if( j.rests == "sort" ){
        sortVal = true;
        this.setState({ sorting : j })
      }

      else {
          if( j.rests == "cost" )
              query = query + 'high=' + j.value[1] + '&&low=' + j.value[0];
          else {
              query = query + j.rests + '=' + j.value;
          }
          query = query + '&&'
      }
    } , this);

    if(sortVal){
      this.setState({ change : sortVal })
    }
    else {
      this.setState({ change : sortVal })
    }

    return query;
  }

  sortResults(e){

    var sortFunction;
    if( e.value == "CostD" ){
      sortFunction = function( a , b) {
          return b.cost - a.cost;
      }
    }
    else if ( e.value == "CostA" ) {
      sortFunction = function ( a , b ) {
          return a.cost - b.cost;
      };
    }
    else if ( e.value == "Recent" ) {
      sortFunction=function ( a , b) {
          return b.id - a.id;
      };
    }
    else if ( e.value == "Rating" ) {
      sortFunction=function ( a , b) {
          return b.avg_rating - a.avg_rating;
      };
    }

    return sortFunction;

  }

  removeFilter( val , fil ){

      this.setState( { fetched : false , latlng3 : undefined } )

    var prevFilters = [];
    prevFilters = this.state.filters;

    var index = prevFilters.findIndex(function(j){

      if(fil != "cost")
          return(( j.rests == fil ) && ( j.value == val ));
      else {
        return(( j.rests == fil ) && ( j.value[0] == val[0] ) && ( j.value[1] == val[1] ));
      }
    });

    if(index==-1)
      return;

    var removed = prevFilters.splice(index, 1);

    var q = this.formQuery(prevFilters);
    q = q.substr( 0 , q.lastIndexOf( "&&" ));


    const url = BASE_URL + '?' + q;
    console.log(url);
    fetch( url ,
    {
      method : 'GET',
      headers : {
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
   })
   .then( response => response.json() )
   .then((json) => this.setState({ filters : prevFilters , resRet : json , fetched : true }));
  }

  componentDidMount() {
    //const url = encodeURI(`${BASE_URL}`);
    var e = this.props.params;
    if(e.rests == "new")
        this.newRes();
    else if(e.rests == "latlng"){
      this.nearByRes();
    }
    else{
      this.search(e);
    }
   }

   allRes(){
     window.location.reload();
   }

  render()  {

    var resList = this.state.resRet;
    var rests;

    if ( resList.length == 0 && this.state.fetched ) {

      rests =
          <div className = "NoRes">
            <div id = "NoResText" >
              <i className="fa fa-frown-o" aria-hidden="true"></i> Sorry no results matched your search
            </div>

            <hr id = "br1" />

            <div className = "clearfix">
              <Link id = "AllResBtn" onClick = {this.allRes} to= "/Listmapview/1/1" >
                  See All Restaurants >
              </Link>
            </div>
          </div>
    }

    else {

      rests = <Lists resHovered = {this.hovered} restList = {resList} page = {"restaurants"}/>
    }

    if ( !this.state.fetched ) {
      rests = <h1 className = "centr" ><i id = "newSpin" className = "fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i></h1>
    }

    return(
      <div style = {{overflow : "hidden"}}>
          <div className = "container-fluid" id = "bc" >
              <div className = "row">
                  <div className = "col-sm-12">
                    <Snav nearByRests = {this.nearByRes} newRests = {this.newRes} handleSearchFilters = {this.search}/>
                  </div>
              </div>
          </div>

          <div className = "container-fluid" id = "mbg">
              <div className = "row">
                  <div className = "col-md-2 col-sm-2" id = "filtersCol">
                      <Filters clearAll = {this.state.cleared} searchFilters = {this.search} removeFilter = {this.removeFilter}/>
                  </div>
                  <div className = "col-md-6 col-sm-10">
                      {rests}
                  </div>
                  <div className = "col-md-4 col-sm-12">
                      <div> <Viewmap hovCoord = {this.state.latlng3} restlist = {resList} /></div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default Listmapview;
