import React, { Component } from 'react';
import Navbar from './Navbar';
import './Owner.css';
import Newe from './Newe';
import Lists from './Lists';
import ChangePass from './ChangePass';
import { Router,Link,hashHistory } from 'react-router';
import headTags from 'head-tags';

class Owner extends Component {
  constructor() {
    super();
    this.state={
      clicked:false,
      resRet:[],
      auth_token:localStorage.getItem("auth_token"),
      fetched : false
    }
    this.getRestaurants=this.getRestaurants.bind(this);
    this.signout=this.signout.bind(this);
    this.changePass = this.changePass.bind(this);
    // this.expiry = this.expiry.bind(this);
  }

  changePass(e){

    document.getElementById('chgPassModal').style.display = "block";
  }

  signout(ll){

    var auth = "Bearer "+this.state.auth_token;
    var that = this
    localStorage.auth_token='';
    localStorage.role="";
    localStorage.uname="";
    localStorage.oid="";
    fetch('http://localhost:9000/ownerSignOut',
    {
      method: 'GET',
      headers:{
        "Authorization":auth,
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>console.log(response.status))
     .then(function(){
       localStorage.auth_token='';
      //  that.setState({signedOut:true})
      //  window.location.replace(ll);
     });
  }

  getRestaurants(e){
    var that=this;
    var auth = "Bearer "+this.state.auth_token;
    console.log(auth);

    fetch('http://localhost:9000/ownersRes',
    {
      method: 'GET',
      headers:{
        "Authorization":auth,
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then( function(response) {
       if( response.status == 401 ){
         localStorage.auth_token = '';
         localStorage.role = "";
         localStorage.uname = "";
         window.alert( "Time period expired. Please login again" );
         that.setState({ resRet : undefined});
         window.location.reload();
       }
       return response.json()
     })
     .then( (json) =>  that.setState({ resRet : json , fetched : true}));
  }

  componentDidMount(){

    if(localStorage.auth_token)
        this.getRestaurants();
  }

  render() {

	var resList=this.state.resRet;
    // this.sortResults(this.state.sorting)
    // console.log(this.state.filters);
	const options = {
		identifyAttribute: 'head-manager'
	  }
	  const headTitle = headTags({
		titleTemplate() {
		return 'Owner Profile'
	  },
	  meta: [
		{charset: 'utf-8'}
	  ]
	  }, options)

	  headTitle.title.toString()
	  headTitle.title.mount()

    var resList = this.props.children?"listHidden":"listVisible"

    var rests;

    if ( this.state.resRet.length == 0 && this.state.fetched ) {

      rests =
          <div className = "NoRes">
            <div id = "NoResText" >
              <i className="fa fa-frown-o" aria-hidden="true"></i> Sorry you have no restaurants to display
            </div>

            <hr id = "br1" />

            <div className = "clearfix">
              <Link id = "AllResBtn" to= "/Owner/Add" >
                  Click to Add Restaurants >
              </Link>
            </div>
          </div>
    }
    else {

      rests = <Lists restList={this.state.resRet} page={"owner"}/>
    }

    if( !this.state.fetched ){
      rests = <h1 className = "centr" ><i id = "newSpin" className = "fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i></h1>
    }

    if(localStorage.auth_token){
    return (
      <div>
        <Navbar logout={this.signout}/>
      <div id="wrapper">
        <div id="sidebar-wrapper">
          <ul className="sidebar-nav">
            <li className="sidebar-brand">
              <Link to="/Owner">
                {localStorage.uname}
              </Link>
            </li>
            <li>
                <Link to="/Owner/Add" onClick={this.expiry}>Add Restaurant</Link>
            </li>
            <li>
                <Link to="/Owner" onClick={this.getRestaurants}>My Restaurants</Link>
            </li>

            <li>
                <Link onClick={this.changePass} >Change Password</Link>
            </li>

        </ul>
    </div>
    <div id="page-content-wrapper">
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-12 bgPad">
                  <ChangePass />
                  <div id={resList}>
                    {rests}
                  </div>
                  {this.props.children}
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
  );
}
else {
  return(
      <div>{hashHistory.push('/')}</div>
  );
}
}
}

export default Owner;
