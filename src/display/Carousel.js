import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';

class Carousel extends Component{
  constructor() {
    // In a constructor, call `super` first if the className extends another className
    super();
}



render(){
  var myCarousel=(this.props.MenuCarousel)?"meCarousel":"myCarousel";
  var targetD=(this.props.MenuCarousel)?"#meCarousel":"#myCarousel";
  if(this.props.imagesList){
  var slides=this.props.imagesList.map(function(j,i){
    var active=(i==0)?"active":"";
    return(
      <li data-target={targetD} data-slide-to={i} key={i} className={active}></li>
    );
  });

  var imagesList1=this.props.imagesList.map(function(j,i){
    var active=(i==0)?"active":"";
    return(
      <div className={"item "+active} key={i}>
        <a href={j} target="_blank"><img className="adjusting" src={j} alt="Chania"/></a>
      </div>
    );
  });
}
return(
  <div id={myCarousel} className="carousel slide" data-ride="carousel">

    <ol className="carousel-indicators">
      {slides}
    </ol>


    <div className="carousel-inner" role="listbox">
      {imagesList1}
    </div>
    <a className="left carousel-control" href={targetD} role="button" data-slide="prev">
      <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span className="sr-only">Previous</span>
    </a>
    <a className="right carousel-control" href={targetD} role="button" data-slide="next">
      <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span className="sr-only">Next</span>
    </a>
  </div>
    );
  }
}
export default Carousel;
