import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.css';
import {Link} from 'react-router';
class Navibar extends Component {
  constructor() {
    super();
    this.state = {
      collapsed: true,
    };
  }
  toggleCollapse() {
      const c= !this.state.collapsed;
      this.setState({collapsed:c});
  }

handle

  render() {
    const { collapsed } = this.state;
    const navClass = collapsed ? "collapse" : "";
    return (


      <nav id="navy" ref="navbar" className="navbar navbar-default navbar-custom  ">
          <div className="container">
              <div className="navbar-header ">
                  <button type="button" className="navbar-toggle" onClick={this.toggleCollapse.bind(this)} data-target="#bs-example-navbar-collapse-1">
                      <span className="sr-only">Toggle navigation</span> Menu <i className="fa fa-bars"></i>
                  </button>
                  <a className="navbar-brand" href="#">HungerQuest</a>
              </div>

              <div className={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                  <ul className="nav navbar-nav navbar-right visible-xs">
                  </ul>
              </div>
          </div>
      </nav>
    );
  }
}

export default Navibar;
