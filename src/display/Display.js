import React, { Component } from 'react';
import Snav from '../Snav';
import Carousel from './Carousel';
import './Display.css';
import 'font-awesome/css/font-awesome.css';
import Rmap from './Rmap';
import Login from '../Login.js'
import Rater from 'react-rater'
import headTags from 'head-tags'
import OpenClose from '../OpenClose';
/*
 // reflect all tags in dom
head.mount()

// get string of all tags
head.toString()*/

class Display extends Component {
  constructor() {
    super();

    this.handleTime=this.handleTime.bind(this);
    this.handleRating=this.handleRating.bind(this);
    this.handleRate=this.handleRate.bind(this);
    this.handleFav=this.handleFav.bind(this);
    this.handleMenu=this.handleMenu.bind(this);
    this.addFav=this.addFav.bind(this);
    this.loginUser=this.loginUser.bind(this);
    this.disableButton=this.disableButton.bind(this);
    this.state = {
      res : [],
      Fav : [],
      rating :0,
      logout : false,
      orrf : false
    };
  }

  componentDidMount(){
    document.getElementById("disableRating").className = "btn btn-primary btn-block disabled";
     this.disableButton();
    //disable the buttons

  }

  componentWillMount() {
    var e = this.props.params.resRet;

    fetch('http://localhost:9000/restaurants?id='+e+'',
    {
      method : 'GET',
      headers :{
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
     })
      .then(response => response.json())
      .then((json) => this.setState({res : json}))
      .then(this.addFav())
      .then(this.disableButton());

  }

  addFav(){
    var e = this.props.params.resRet;
    var that = this;

    fetch('http://localhost:9000/favourites',
    {
      method : 'GET',
      headers :{
        "Authorization" : "Bearer " + localStorage.auth_token,
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
     })
    .then( response => response.json())
    .then((json) => that.setState({ Fav : json }))
    .then(function(v){

        document.getElementById("MyBttn").setAttribute("class","btn btn-default ");
        document.getElementById("BeforeFav").setAttribute("class","addAs");

        that.state.Fav.map(function(j,i){
          if(e==j.id){
            document.getElementById("MyBttn").setAttribute("class","btn btn-danger")
            document.getElementById("BeforeFav").setAttribute("class","removeAs");
          }
        });
    });
  }

  loginUser(p){
    document.getElementById('loginModal').style.display = "block";
    document.getElementById( 'errMsg' ).innerHTML = "Signin as user to use favourites feature and give Review & rating ";
    document.getElementById( 'ownercheck1' ).disabled = true;
    document.getElementById( 'usercheck1' ).checked = true;
  }

  handleMenu(e){
    document.getElementById( 'MenuCar' ).style.display = "block";
  }

  handleClose(){
    document.getElementById( 'MenuCar' ).style.display = "none";
  }

  handleFav(v){
    var that=this;
    var i =this.props.params.resRet;
    var form=JSON.stringify({
       rid :i
    });

    fetch('http://localhost:9000/favourites',
    {
      method: 'POST',
      headers:{
        "Authorization":"Bearer "+localStorage.auth_token,
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
      body:form
    })
    .then(function(response){
      if(response.status != 200  ){
        if(localStorage.role=="owner"){
            var a = window.confirm("You are signed in as Owner , favourites feature is available as User \n Do you want to Logout and Signin as user ")
            if(a){
                that.setState({logout:true});
                that.setState({orrf:true});
                window.setTimeout(that.loginUser(),10000);
            }
        }
      }
      else{
          that.addFav();
      }
    })
    .then(function(n){
        if(!localStorage.auth_token){
          console.log("in fav no auth");
          that.loginUser();
        }
    })
  }

  handleRating(e){
    var i =this.props.params.resRet;
    var r=this.state.rating;
    var that = this;

    var form=JSON.stringify({
         rid :i,
         uname: localStorage.getItem("uname"),
         msg: document.getElementById("textin").value
    });

    fetch('http://localhost:9000/reviews',
    {
        method: 'POST',
        headers:{
           "Authorization":"Bearer "+localStorage.auth_token,
           "Content-Type":"application/json",
           "Accept":"application/json"
         },
         body:form
        })
        .then(response=>console.log(response.status));


        var forms=JSON.stringify({
             rid :i,
             rating: r
           });

        fetch('http://localhost:9000/ratings',
        {
          method: 'POST',
          headers:{
            "Authorization":"Bearer "+localStorage.auth_token,
            "Content-Type":"application/json",
            "Accept":"application/json"
          },
          body:forms
         })
         .then(function(response){
           if(response.status != 200  ){
             if(localStorage.role=="owner"){
                     var a = window.confirm("You are signed in as Owner , favourites feature is available as User \n Do you want to Logout and Signin as user ")
                         if(a){
                           that.setState({logout:true});
                           that.setState({orrf:true});
                           window.setTimeout(that.loginUser(),10000);
                         }}
           }
           else{
             window.location.reload();}
         })
         .then(function(v){

           if(!localStorage.auth_token)
           {

             that.loginUser();
           }
         });
}

  handleTime(e){
    var hours = e.slice(0,e.indexOf(':'));
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    var minutes=e.slice(e.indexOf(':')+1,e.lastIndexOf(':'))
    var e = hours + ':' + minutes + ' ' + ampm;
    return e;
  }

  handleRate(e){
    var that=this;
    //check text box is filled
      if(document.getElementById("textin").value)
      {
        document.getElementById("disableRating").className = "btn btn-primary btn-block"
      }
    if (e.originalEvent.type === 'click')
    {
          that.setState({
           rating: e.rating
         });
    }
  }

  disableButton(){

    if(this.state.res.length!=0)  {
          if(this.state.res.fbUrl==""){
            document.getElementById("disFab").style.display="none";
          }

          if(this.state.res.homeUrl==""){
            document.getElementById("disWeb").style.display="none";
          }
          if(this.state.res.menuPath==""){
            document.getElementById("disMenu").style.display="none";
          }}
  }

  render() {
    var res = this.state.res;
    var head = res.name;
    var rate = res.avg_rating?res.avg_rating:"Not Rated";
    var fb = res.fbUrl;
    var phn = res.phn;
    var lat= res.latitude;
    var lng = res.longitude;
    var website = res.homePageUrl;
    var rating = "star"+this.state.rating;
    var googleDirection = "https://www.google.com/maps/dir/Current+Location/"+this.state.res.name+"+"+this.state.res.area

	const options = {
		identifyAttribute: 'head-manager'
	  }
	  const headTitle = headTags({
		titleTemplate() {
		return 'Restaurant Page ' + head
	  },
	  meta: [
		{charset: 'utf-8'}
	  ]
	  }, options)

	  headTitle.title.toString() // <title>foo - My Website</title>
	  headTitle.title.mount() // document.title changed!

  //images
    var images=res.photosPath;
    if(images){
      var imagesList=images.map(function(j,i){
        return(<img src={""+j} id="carouselImg" key={i}/>);
      });
    }
  //REVIEWS
  if(res.reviews){
  var reviews = res.reviews.map(function(a,b){
  var rating =  res.ratings.map(function(c,d){
      if(c.uname==a.uname){
      return(
        <div id="uReview" key={b}>
          <span id="uname">{a.uname}</span><span className="pull-right"><Rater id="sisin" rating={c.rating}  interactive={false}/></span>
          <div id="umsg">{a.msg}</div>
          <hr/>
        </div>
      );
    }
    })
    return(<div key={b}>{rating}</div>);
    })
  }

//testing
this.disableButton();
//endtest

  //Information about Restaurant
    var infor = [res].map(function(j,i){
      if(j.cuisine){
        var cuisines= j.cuisine.map(function(c,k){
          return(
            <div key={k}>{c.cuisine}</div>
          );
        });
      }

      if(j.highlights){
        var highlights= j.highlights.map(function(c,k){
          return(
            <div key={k}>{c}</div>
          );
        });
      }

          //converting into am pm
          var oTime;
          var cTime;

          if(j.openTime){

            oTime=this.handleTime(j.openTime);
            cTime=this.handleTime(j.closeTime);
          }

      return(

        <div className="row" key={i}>
          <div className="col-lg-4 col-md-6 col-sm-12">
            <div className="df">Address: </div>
            <div>{j.address}</div>
            <br/>

            <div className="df">timings: </div>
            <div>{oTime} - {cTime}  <OpenClose start = { j.openTime } end = { j.closeTime } /></div>
            <br/>

            <div className="df">Cost: </div>
            <div>{j.cost}</div>
            <br/>
          </div>

          <div className="col-lg-4 col-md-6 col-sm-12">
            <div className="df">Cuisine: </div>
            {cuisines}
            <br/>
            <div className="df">Phone Number: </div>
            <div>{j.phn}</div>
            <br/>
          </div>

          <div className="col-lg-4 col-md-6 col-sm-12">
            <div className="df">Highlights: </div>
            <div>{highlights}</div>
            <br/>
          </div>
        </div>
      );
    },this);



return (


  <div>
    <Snav oSignout={this.state.logout} inDisplay={true}/>
      <Carousel imagesList={this.state.res.photosPath}/>
    <div className="container wide">
        <div className="row">
            <div className="col-md-6  col-sm-12" >
                <h2 >{head}</h2>
            </div>
            <div className="col-md-6 col-sm-12" id="mee">
                <span id="hee">Rating: </span><span id="rateVal">{rate}</span>
                  <button onClick={this.handleFav} className="btn btn-default" id="MyBttn"><i className="fa fa-bookmark" aria-hidden="true"></i>&nbsp;<span id="BeforeFav">Favourite</span></button>
            </div>
        </div>
    </div>
    <div className="container-fluid rbg">
          <div className="row mar">
              <div className="col-md-6  col-sm-12 ">
                  <div className="bg bx">
                    <h3 className="centr">INFORMATION</h3>

                        <div id="overflow" className=" container-fluid ">
                          {infor}
                        <div className="row">
                          <div className="col-sm-12 ">
                            <div className="pull-right centr">
                          <button id="disDir" className="myDirection dis"><a href={googleDirection} target="_blank"><i className="fa fa-arrows" aria-hidden="true"></i><br/>Direction</a></button>
                          <button id="disMenu" className="Mymenu dis" onClick={this.handleMenu}><i className="fa fa-book" aria-hidden="true"></i><br/>Menu</button>
                          <button id="disPhone" className="phone dis"><a href="tel:{phn}"><i id="ph" className="fa fa-phone" aria-hidden="true"></i><br/>CALL</a></button>
                          <button id="disFab" className="fab dis"><a href={fb} target="_blank"><i id="fbb" className="fa fa-facebook" aria-hidden="true" ></i><br/>Facebook</a></button>
                          <button id="disWeb" className="web dis"><a href={website} target="_blank"><i id="web" className="fa fa-globe" aria-hidden="true"></i><br/>WEB</a></button>
                        </div>
                        </div>
                        </div>
                    </div>
                  </div>
              </div>
              <div className="col-md-6  col-sm-12 ">
                <div className="bg bx">
                  <h3 className="centr">LOCATION</h3>
                    <Rmap lat={lat} lng={lng} head={head} area={this.state.res.area} />
                </div>

              </div>

              <div className="col-md-12  col-sm-12 ">
                <div className="bg bx mrt" id="reviewHolder"><h2 className="centr"> REVIEWS </h2>
                <div id="ForReview">
                  {reviews}
                  <div id="marin">
                    <textarea id= "textin" className="form-control" placeholder="Give Your Review" />
                  </div>
                </div>
                <div  id="input1">
                <Rater id="hcen" onRate={this.handleRate} rating={this.state.rating}  />
                <span id="st" className={rating}></span>
                  </div>
                  <div className="col-xs-6 col-xs-offset-3" id="rateButton">
                <button id="disableRating" type="submit" className="btn btn-primary btn-block " onClick={this.handleRating}>Send Review & Rating</button>
                </div>
            </div>
              </div>
              <div id="MenuCar" className="modal">
                <div className = "close" id="MyClosing" onClick = { this.handleClose }> &times; </div>
                <div id="MenuMar">
                  <Carousel MenuCarousel={true} imagesList={this.state.res.menuPath}/>
                </div>
            </div>
        </div>
    </div>

  </div>
    );
  }
}

export default Display;
