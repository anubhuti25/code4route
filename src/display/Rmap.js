import React, { Component } from 'react';
var map;
var markerr;
var marker1;
var myLatlng;
var infowindow =new google.maps.InfoWindow();
var bounds1 = new google.maps.LatLngBounds();

class Rmap extends Component {
  constructor(props) {
    // In a constructor, call `super` first if the class extends another class
    super(props);
    this.directions = this.directions.bind(this);
  }

  componentDidMount(){
    // myLatlng = new google.maps.LatLng(17.3850,78.4867);
    map = new google.maps.Map( this.refs.map, {
      zoom : 8,
      scrollwheel : false
    });

    //initial position of restaurant
    markerr = new google.maps.Marker({
      map : map,
    });

    //Current Location marker
    marker1 = new google.maps.Marker({
      map: map,
    });
    //changing icon for current location
    marker1.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

    //setting info window content for current location marker
    var infowindow = new google.maps.InfoWindow({
        content: '<div class="myFont">My location</div>'
    });

    marker1.addListener('mouseover', function() {
      infowindow.setContent('<div class="myFont">My location</div>')
          infowindow.open(map, marker1);
    });
  }

  componentWillReceiveProps(nextProps){

    var that = this;
    if ( navigator.geolocation && nextProps.lat && nextProps.lng ) {
      navigator.geolocation.getCurrentPosition( function( position ) {
        var pos = {
          lat : position.coords.latitude,
          lng : position.coords.longitude
        };

        myLatlng = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );

        //setting postion of marker to current location
        map.setCenter(pos);
        marker1.setPosition(pos);
        marker1.setAnimation(google.maps.Animation.DROP);
        bounds1.extend(pos);
        map.fitBounds(bounds1);

        if( nextProps.lat &&  nextProps.lng && myLatlng){
          var pos1 = new google.maps.LatLng( nextProps.lat , nextProps.lng );
          that.directions(pos1);

          bounds1.extend(pos1);
          map.fitBounds(bounds1);
        }



      },
      function() {
          // handleLocationError(true, infoWindow, map.getCenter());
          myLatlng = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );
          var pos1 = new google.maps.LatLng( nextProps.lat , nextProps.lng );
          that.directions(pos1);
          map.setCenter(myLatlng);
          marker1.setAnimation(google.maps.Animation.DROP);
          marker1.setPosition(myLatlng);
          bounds1.extend(myLatlng);
          map.fitBounds(bounds1);
      });
    }
    else {
      // handleLocationError(false, infoWindow, map.getCenter());
    }
  }

  directions(a){

    var that = this;
    var dist , time;

    markerr.setPosition(a);
    markerr.setAnimation(google.maps.Animation.DROP);

    bounds1.extend(a);
    map.fitBounds(bounds1);
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers:true });

    directionsDisplay.setMap(map);
    directionsService.route({
        origin: myLatlng,
        destination: a,
        travelMode: 'DRIVING'
      }, function(response, status) {
          if (status === 'OK') {
            dist = response.routes[0].legs[0].distance.text;
            time = response.routes[0].legs[0].duration.text;


            directionsDisplay.setDirections(response);

          } else {
            window.alert('Directions request failed due to ' + status);
          }
      });


      markerr.addListener( 'mouseover',
        function() {
          var contentString = '<div><div class="myFont">' + that.props.head + '</div>'+
              '<div>'+'<span class="boldin">Distance: </span>' + dist + '</div>'+
              '<div>'+'<span class="boldin">Duration: </span>' + time + '&nbsp;<i class="fa fa-car"></i></div>'+
              '</div>';
          infowindow.setContent(contentString);
          infowindow.open(map,markerr);
      });

      google.maps.event.addListener(infowindow,'closeclick',function(){
          map.fitBounds( bounds1 );
          // directionsDisplay.setMap(null);
      });

  }

  render() {
    return (
      <div id="Rmap">
        <div ref="map" style={{height:260}}></div>
      </div>
    );
  }
}

export default Rmap;
