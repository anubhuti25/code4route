import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './Navigate.css';
import Login from './Login.js';
import Signup from './Signup.js';
import 'bootstrap/dist/css/bootstrap.css';
import {Link} from 'react-router';
var scrollTop=0;
var that;
class Navigate extends Component {
  constructor() {
    super();
    this.state = {
      collapsed: true,
      navScroll:false,
    };
    this.logout=this.logout.bind(this);
    this.closeLog=this.closeLog.bind(this);
  }

  closeLog(){
    document.getElementById('profileLi').style.display="block";
    this.forceUpdate();
  }


  logout(){

    var auth = "Bearer "+localStorage.auth_token;
    localStorage.auth_token=''
    var signO=localStorage.role=="owner"?"ownerSignOut":"userSignOut";
    localStorage.role='';
    fetch('http://localhost:9000/'+signO,
    {
      method: 'GET',
      headers:{
        "Authorization":auth,
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>console.log(response.status))
     .then(function(){
       document.getElementById('profileLi').style.display="none";
       localStorage.auth_token='';
      //  window.location.replace(ll);
     });

  }

  toggleCollapse() {
      const c= !this.state.collapsed;
      this.setState({collapsed:c});
  }

  handleScroll(ev) {
    scrollTop=ev.srcElement.body.scrollTop;
    if(scrollTop>=100){
        that.setState({navScroll:true});
    }
    else{
        that.setState({navScroll:false});
    }
  }

  componentDidMount() {
    that=this;
    window.addEventListener('scroll', this.handleScroll);
    if(localStorage.auth_token){
      document.getElementById('profileLi').style.display="block";
    }

    window.addEventListener('storage', function(e) {
      window.location.reload();
    });

  }

  componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
  }

  dd(){
    console.log("here");
  }

  render() {

    const { collapsed } = this.state;
    const navClass = collapsed ? "collapse" : "";

    var role1 = localStorage.role == "owner" ? "/Owner" : "/User"

    const profileNav=(navClass=="collapse")?<i id="profBtn" className="fa fa-user fa-2x" aria-hidden="true"></i>
                      :<Link to={role1} >Profile</Link>

    const navScroll = this.state.navScroll?"scrolled-nav":"";
    const signIn=localStorage.auth_token?'':<Login closeMod={this.closeLog}/>
    const signUp=localStorage.auth_token?'':< Signup/>
    const prof=localStorage.auth_token?profileNav:''
    var FavVis=(localStorage.role=="user")?<Link to = "/User" className="page-scroll smaller" > Favourites </Link>:"";


    return (

      <nav id="mainNav" ref="navbar" className={"navbar navbar-default navbar-custom navbar-fixed-top "+navScroll}>
          <div className="container">

              <div className="navbar-header page-scroll">
                  <button type="button" style = {{marginRight : "15px"}} className="navbar-toggle" onClick={this.toggleCollapse.bind(this)} data-target="#bs-example-navbar-collapse-1">
                      <span className="sr-only">Toggle navigation</span> <i className="fa fa-bars"></i>
                  </button>
                  <a className="navbar-brand page-scroll" href="#">HungerQuest</a>
              </div>


              <div className={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                  <ul className="nav navbar-nav navbar-right">
                      <li className="hidden">
                          <a href="#"></a>
                      </li>

                      <li>
                        {signUp}
                      </li>

                      <li>
                          {signIn}
                      </li>
                      <li>
                          {FavVis}
                      </li>
                      <li>
                          <a className="page-scroll smaller" href="#Newr">New</a>
                      </li>
                      <li>
                          <a className="page-scroll smaller" href="#HomeMap">Nearby</a>
                      </li>
                      <li>
                          <a className="page-scroll smaller" href="#contact">Contact</a>
                      </li>

                      <li  className="smaller hidden-xs" id="profileLi">{(localStorage.auth_token)&&(navClass=="collapse")?<a id="profile">{localStorage.uname}</a>:''}{prof}

                        <ul className="dropdown2" id="dd2">
                          <li className="link2"><Link to={(localStorage.role=="owner")?"/Owner":"/User"}>My Profile</Link></li>
                          <hr className="bre"/>
                          <li className="link2"><Link to="/" onClick={this.logout}>Logout</Link></li>
                        </ul>
                      </li>

                      <li className = "visible-xs smaller">
                          {prof}
                      </li>

                      <li className = "visible-xs smaller">
                          {localStorage.auth_token?<Link to="/" onClick={this.logout}>Logout</Link>:''}
                      </li>

                  </ul>
              </div>

          </div>
      </nav>
    );
  }
}

export default Navigate;
