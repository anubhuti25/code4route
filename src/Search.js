import React, { Component } from 'react';
import "./Search.css";
import { Link } from 'react-router';
import 'bootstrap/dist/css/bootstrap.css';

class Search extends Component{
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
    this.state = {
      rests : '1',
      value : ['1']
    };
    this.search = this.search.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSearch = this.handleSearch.bind(this);

  }

  search(e){
    var param=e.currentTarget.id;
    var val=document.getElementById('search_box').value;
    this.setState({rests : param});
    document.getElementById('search_concept').innerHTML=""+param;

  }
  handleSearch(e){
        if( this.props.filtMob ){
          this.props.search(this.state);
          document.getElementById('filtersCol').style.display = "none";
        }

  }

  handleInput(e){
    var val=e.currentTarget.value;
    this.setState({value:val});
  }


  render() {
    return(
      <div className="container">
        <div className="row">
          <div className="col-xs-10 col-xs-offset-1">
		        <div className="input-group">
                <div className="input-group-btn search-panel">
                    <button id="search_btn" className="btn btn-default dropdown-toggle"  data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">
                    	<span id="search_concept">Search by</span> <span className="caret"></span>
                    </button>

                    <ul id="dd1" className="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a onClick={this.search} id="area">Area</a></li>
                      <li><a onClick={this.search} id="cuisine">Cuisine</a></li>
                      <li><a onClick={this.search} id="name">Name</a></li>
                    </ul>
                </div>

                <input id="search_box" type="text" className="form-control" name="x" placeholder="Search term..." onChange={this.handleInput}  />
                <span className="input-group-btn ">
                    <Link onClick = {this.handleSearch} to={"/Listmapview/"+this.state.rests+"/"+this.state.value}><button id="search_icon"  className="btn btn-default" type="button"><span className="glyphicon glyphicon-search"></span></button></Link>
                </span>
            </div>
          </div>
	      </div>
      </div>
    );
  }
}


export default Search;
