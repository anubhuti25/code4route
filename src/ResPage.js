import Owner from './Owner';
import 'bootstrap/dist/css/bootstrap.css';
import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { Link , hashHistory } from 'react-router';
import './Add.css';
import MapSample from './MapSample';
import cuisine from './cuisine';
import DropDemo from './DropDemo';
import Uploads from './Uploads';
import headTags from 'head-tags';
var that;
const BASE_URL = 'http://localhost:9000/restaurants';
var num=0;
var rid;
class ResPage extends Component {
  constructor() {
    super();
	  this.state={
      lat:'',
      lng:'',
      files:[],
      menus:[],
      area:[],
      areaRes:'',
      areaSelected:'',
      cuisine:[]
    }

    this.handleClick=this.handleClick.bind(this);
    this.removeBtn=this.removeBtn.bind(this);
    this.handleUpdate=this.handleUpdate.bind(this);
    this.getLatLng=this.getLatLng.bind(this);
    this.handleDelete=this.handleDelete.bind(this);
    this.handleFileUpload=this.handleFileUpload.bind(this);
    this.handleFileDelete=this.handleFileDelete.bind(this);
    this.handleAreas=this.handleAreas.bind(this);
    this.validateText = this.validateText.bind(this);
  }

  componentDidMount(){

    fetch('http://localhost:9000/cuisines',
    {
      method: 'GET',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
     })
     .then(response=>response.json())
     .then((json)=>this.setState({cuisine:json}));

     fetch('http://localhost:9000/areas',
     {
       method: 'GET',
       headers:{
         "Content-Type":"application/json",
         "Accept":"application/json"
       },
      })
      .then(response=>response.json())
      .then((json)=>this.setState({area:json}));


    that=this;
    rid=that.props.params.textvalue;
    const url = encodeURI(`${BASE_URL}?id=${rid}`);
    fetch(url,
    {
      method: 'GET',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>response.json())
     .then(function(json){
      //  console.log(json);
       document.getElementById('arealist').innerHTML=json.area;
       that.setState({lat:json.latitude,lng:json.longitude,files:json.photosPath,menus:json.menuPath,areaRes:json.area});
       document.getElementById('Name').value=json.name;
       document.getElementById('address').value=json.address
       document.getElementById('cost').value=json.cost;
       document.getElementById('openTime').value=json.openTime.slice(0,json.openTime.lastIndexOf(':'));
       document.getElementById('closeTime').value=json.closeTime.slice(0,json.closeTime.lastIndexOf(':'));
       document.getElementById('phn').value=json.phn;
       document.getElementById('hmUrl').value=json.homePageUrl;
       document.getElementById('fbUrl').value=json.fbUrl;
       document.getElementById('description').value=json.description;
       document.getElementById('latitude').value=json.latitude;
       document.getElementById('longitude').value=json.longitude;
       for(var i=0;i<json.cuisine.length;i++){
         document.getElementById(json.cuisine[i].cuisine).checked=true;
       }
       for(var i=0;i<json.highlights.length;i++){
         that.handleClick(json.highlights[i]);
       }
     });
  }

  handleFileDelete(e,l){

    var f=[];
    if(l=="menu"){
      f=this.state.menus
    }
    else {
      f=this.state.files;
    }

    var deletedFile=f[e];
    console.log("deleted image is"+f[e]);

    f[e]=undefined;

    fetch(deletedFile,
    {
      method: 'DELETE',
      headers:{
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>console.log(response.status))
     .then(function(e){document.getElementById('myFile').value="";});

     if(l=="menu"){
       this.setState({menus:f});
     }
     else {
       this.setState({files:f});
     }

  }

  getLatLng(l1,l2){
    this.setState({lat:l1,lng:l2});
  }

  handleFileUpload(e,ll){

    console.log("hello");
    fetch("http://localhost:9000/images", {
      method: "POST",
      body: e
    })
    .then(response=>response.json())
    .then(function(json){
      var a=[];
      if((ll=="menu")&&(that.state.menus)){
          a=[...that.state.menus];
          a.push(json);
      }
      else if((ll!="menu")&&(that.state.files)){
          a=[...that.state.files]
          a.push(json);
        }
        else {
          a=[json];
        }

        console.log(a);
        // console.log(ll);

        if(ll=="menu"){
          that.setState({menus:a});
        }

        else {
          that.setState({files:a});
        }

    });

  }

  handleDelete(e){

    var dele=window.confirm("Do you wish to delete this restaurant");
    if(!dele){
      return;
    }

    var li=document.getElementById("delLink").href="http://localhost:8081/#/Owner/";


    fetch('http://localhost:9000/restaurants/'+rid+'',
    {
      method: 'DELETE',
      headers:{
        "Authorization":"Bearer "+localStorage.auth_token,
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>response.text())
     .then(function( results ){
       if( results == "Time period expired. Please login again" || results == "in action" ){
         localStorage.auth_token = '';
         localStorage.role = "";
         localStorage.uname = "";
         window.alert( "Time period expired. Please login again" );
       }

       window.location.reload();
       window.location.replace(li);
     });


  }

  validateText(val,id){
    if(!document.getElementById(val).value){
      document.getElementById(id).innerHTML = 'Please enter '+val;
      console.log(val+'not valid');
      return true;
    }
    document.getElementById(id).innerHTML = '';
    return false;
  }

  handleUpdate(e){

    // Get the selected cuisines

    console.log("in update");


    var li=document.getElementById("upLink").href="http://localhost:8081/#/Owner/";



    var cuisineSelected=[];
    var highlights1=[];
    for(var i=0;i<=num;i++){
      if(document.getElementById('text'+i)){
        highlights1.push(document.getElementById('text'+i).value);
      }
    }

    //Get all the highlights
    for(var i=0;i<this.state.cuisine.length;i++){
      if(document.getElementById(this.state.cuisine[i].cuisine).checked){
        cuisineSelected.push(this.state.cuisine[i].cuisine);
      }
    }

    //validate
    var valFlag = false;
    if(this.validateText('Name','valName'))
      valFlag = true;
    if(this.validateText('address','valAdd'))
      valFlag = true;
    if(!this.state.areaRes.length)
    {
      valFlag = true;
      document.getElementById('valArea').innerHTML = 'Please select area';
    }
    else{
      document.getElementById('valArea').innerHTML = '';
    }
    if(cuisineSelected.length == 0){
      valFlag = true;
      document.getElementById('valCuisine').innerHTML = 'Please select cuisine';
    }
    else{
      document.getElementById('valCuisine').innerHTML = '';
    }
    if(this.validateText('cost','valCost'))
      valFlag = true;
    if(this.validateText('openTime','valOtime'))
      valFlag = true;
    if(this.validateText('closeTime','valCtime'))
      valFlag = true;
    if(highlights1.length == 0){
      valFlag = true;
      document.getElementById('valHigh').innerHTML = 'Please enter highlights';
    }
    else{
      document.getElementById('valHigh').innerHTML = '';
    }
    if(this.validateText('phn','valPhone'))
      valFlag = true;
    if(this.validateText('description','valDesc'))
      valFlag = true;
    if(!this.state.files.length){
      console.log('photo not valid');
      valFlag = true;
      document.getElementById('valPhotos').innerHTML = 'Please upload restaurant photos';
    }
    else{
      document.getElementById('valPhotos').innerHTML = '';
    }
    if((!this.state.lat) || (!this.state.lng)){
      valFlag = true;
      document.getElementById('valLoc').innerHTML = 'Please give location';
    }
    else{
      document.getElementById('valLoc').innerHTML = '';
    }

    if(valFlag){
      console.log(valFlag);
      return;
    }

    var files1 = this.state.files;
    var files2 = [];

    for(var k=0 ; k<files1.length ; k++){
      if(files1[k]){
        files2.push(files1[k])
      }
    }

    var menus1 = this.state.menus;
    var menus2 = [];

    for(var m=0 ; m<menus1.length ; m++){
      if(menus1[m]){
        menus2.push(menus1[m])
      }
    }

    var upd=window.confirm("Do you want to update");

    if(!upd)
        return;





    //converting form data to json
    var form=JSON.stringify({
      name:document.getElementById('Name').value,
      address:document.getElementById('address').value,
      cuisine:cuisineSelected,
      cost:document.getElementById('cost').value,
      openTime:document.getElementById('openTime').value+":00",
      closeTime:document.getElementById('closeTime').value+":00",
      phn:document.getElementById('phn').value,
      fbUrl:document.getElementById('fbUrl').value,
      homePageUrl: document.getElementById('hmUrl').value,
      description:document.getElementById('description').value,
      latitude:document.getElementById('latitude').value,
      longitude:document.getElementById('longitude').value,
      highlights:highlights1,
      photosPath:files2,
      menuPath:menus2,
      area:this.state.areaRes,
      description:document.getElementById('description').value
    });

    //calling POST for add restaurant REST api
    fetch('http://localhost:9000/restaurants/'+rid+'',
    {
      method: 'PUT',
      redirect:'manual',
      headers:{
        "Authorization":"Bearer "+localStorage.auth_token,
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
      body:form
     })
     .then(response=>response.text())
     .then(function( results ){
       if( results == "Time period expired. Please login again" || results == "in action" ){
         localStorage.auth_token = '';
         localStorage.role = "";
         localStorage.uname = "";
         window.alert( "Time period expired. Please login again" );
       }

       window.location.reload();
       window.location.replace(li);
     });
  }

  //adding highlights
  handleClick(e){
    num++;
    //creating a new textbox and button
    var element = document.createElement("input");
    var element1=document.createElement("button");
    var element2=document.createElement("span");

    //setting attributes for textbox
    element.setAttribute("type","text");
    element.setAttribute("className","form-control "+num);
    element.setAttribute("id","text"+num);
    if(typeof(e)=='string')
      element.setAttribute("value",e);

    //setting attributes for minus button
    element1.setAttribute("className","btn btn-default minus");
    element1.setAttribute("id",num);
    element1.setAttribute("style","background-color: #fed136;border-color:#fed136;color: white;");
    element1.onclick=this.removeBtn;

    //defining the minus icon
    element2.className="glyphicon glyphicon-minus";

    //appending elements to the form
    var foo = document.getElementById("tb");
    foo.appendChild(element);
    foo.appendChild(element1);

    //appending minus icon to the button
    foo = document.getElementById(num);
    foo.appendChild(element2);
  }

  removeBtn(e){
    //remove the minus button that was clicked and its corresponding textbox
    var foo=document.getElementById("tb");
    foo.removeChild(e.currentTarget);
    foo.removeChild(document.getElementById("text"+e.currentTarget.id));
  }

  handleAreas(e){
    console.log(e.currentTarget.id);
    document.getElementById('arealist').innerHTML=e.currentTarget.id;
    this.setState({ areaSelected : e.currentTarget.id , areaRes : e.currentTarget.id });

  }

  render() {

	var resList=this.state.resRet;
    // this.sortResults(this.state.sorting)
    // console.log(this.state.filters);
	const options = {
		identifyAttribute: 'head-manager'
	  }
	  const headTitle = headTags({
		titleTemplate() {
		return 'Update/Delete Restaurant'
	  },
	  meta: [
		{charset: 'utf-8'}
	  ]
	  }, options)

	  headTitle.title.toString()
	  headTitle.title.mount()

    console.log(this.state.files);
    var cuisines=this.state.cuisine;
    var taskList1=cuisines.map(function(j,i){
      var rObj=j.cuisine;
      return (
        <div className="col-sm-6 col-md-6 col-lg-4" key={i.toString()}>
            <div className="checkbox">
                <label>
                    <input type="checkbox" id={rObj} value={rObj} />{rObj}
                </label>
            </div>
        </div>
      );
    });

    var areas = this.state.area;
    // console.log(areas);
    var areaList = areas.map(function(j,i){
      return(
        <li key={i}><Link onClick={this.handleAreas} id={j.area} to={hashHistory.getCurrentLocation().pathname}>{j.area}</Link></li>
      );
    },this)

    return (
      <div className="container detail" id="cont">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 detail hello" id="hh">
            <form className="form-horizontal" role="form" id="detForm">
                <h2>Restaurant Details</h2>
                <div className="form-group">
                    <label htmlFor="firstName" className="col-sm-3 control-label">Name:</label>
                    <div className="col-sm-9">
                        <input type="text" id="Name" placeholder="Restaurant Name" className="form-control" />
                    </div>
                </div>
                <p id="valName"></p>
                <div className="form-group">
                    <label htmlFor="address" className="col-sm-3 control-label">Address:</label>
                    <div className="col-sm-9">
                        <input type="text" id="address" placeholder="Address" className="form-control" />
                    </div>
                </div>
                <p id="valAdd"></p>
                <div className="form-group">
                    <label htmlFor="Area" className="col-sm-3 control-label">Select Area:</label>
                    <div className="col-sm-9">
                      <div className="dropdown">
                        <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                          <span id="arealist" >Area</span> <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu">
                            {areaList}
                        </ul>
                      </div>
                    </div>
                </div>
                <p id="valArea"></p>
                  <div className="form-group">
                    <label id="ca" className=" control-label col-sm-3">Cuisine:</label>
                    </div>
                    <p id="valCuisine"></p>
                <div id="cuisine1">
                    {taskList1}

                  </div>
                <div className="form-group clear">
                    <label className="col-sm-3 control-label">Cost:</label>
                    <div className="col-sm-9">
                        <input type="text" id="cost" placeholder="Approx for 2" className="form-control" />
                    </div>
                </div>
                <p id="valCost"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Opening Time:</label>
                    <div className="col-sm-9">
                        <input type="time" id="openTime" className="form-control"/>
                    </div>
                </div>
                <p id="valOtime"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Closing Time:</label>
                    <div className="col-sm-9">
                        <input type="time" id="closeTime" className="form-control" />
                    </div>
                </div>
                <p id="valCtime"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Highlights (click on + to add ):</label>
                    <button type="button" className="btn btn-default" onClick={this.handleClick} id="highBtn" >
                        <span className="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                    <div className="col-md-6" id="tb">

                    </div>
                </div>
                <p id="valHigh"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Phone Number:</label>
                    <div className="col-sm-9">
                        <input type="text" id="phn" className="form-control"/>
                    </div>
                </div>
                <p id="valPhone"></p>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Facebook page:</label>
                    <div className="col-sm-9">
                        <input type="url" id="fbUrl" placeholder=" Enter Facebook page Url" className="form-control"/>
                    </div>
                </div>
				<div className="form-group">
                    <label className="col-sm-3 control-label">Home page:</label>
                    <div className="col-sm-9">
                        <input type="url" id="hmUrl" placeholder=" Enter Home page Url" className="form-control"/>
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-sm-3 control-label">Description:</label>
                    <div className="col-sm-9">
                <textarea className="form-control" placeholder="Your Message " id="description"></textarea>
                </div>
            </div>
            <p id="valDesc"></p>
          </form>
            </div>
                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 hello">
                  <div className="form-group">
                        <div className="col-sm-11  detail bg">
                          <h2 className="head1">Upload Photos</h2>
                          <p id="valPhotos"></p>
                          <DropDemo handleFiles={this.handleFileUpload}/>
                          <Uploads images1={this.state.files} handleFiles={this.handleFileDelete}/>
                        </div>
                    </div>

					<div className="form-group">
                        <div className="col-sm-11  detail bg">
                          <h2 className="head1">Upload Menu Photos</h2>
                          <DropDemo menu1={true} handleFiles={this.handleFileUpload}/>
                          <Uploads images1={this.state.menus} menu1={true} handleFiles={this.handleFileDelete}/>
                        </div>
                    </div>
                    <div className="map col-sm-11  detail">
                      <h2>Select your location</h2>
                      <MapSample
                        onMChange={this.getLatLng}
                        area1={this.state.areaSelected}
                        lat1={this.state.lat}
                        lng1={this.state.lng}
                        update1={true}
                      />
                      <p id="valLoc"></p>
					</div>

				</div>
            </div>
            <div className="form-group" id="upDelBtn">
                <div className="col-sm-6">
                    <Link id="upLink" to={hashHistory.getCurrentLocation().pathname}><button type="submit" className="btn btn-primary btn-block" onClick={this.handleUpdate}>Update</button></Link>
                </div>

                <div className="col-sm-6">
                    <Link id="delLink" to={hashHistory.getCurrentLocation().pathname}><button type="submit" className="btn btn-primary btn-block" onClick={this.handleDelete}>Delete</button></Link>
                </div>
            </div>
      </div>

);
}
}

export default ResPage;
