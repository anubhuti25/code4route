import React, { Component } from 'react';
import './MapSample.css'
var marker;
var map;

class MapSample extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
    this.state={
      lat:'',
      lng:'',
      update:false,
      markerdragged:false,
      prevArea:''
    }
//this.showMap=this.showMap.bind(this);
    this.handleLatLng=this.handleLatLng.bind(this);
    this.setLatLng=this.setLatLng.bind(this);
    this.changeArea=this.changeArea.bind(this);
  }

  componentDidMount(){


    var myLatlng = new google.maps.LatLng(17.3850,78.4867);

    var input = document.getElementById('address2');
    var countryRestrict = {'country': 'in'};

    var autocomplete = new google.maps.places.Autocomplete(
      (input), {
              componentRestrictions: countryRestrict
            });

    if(this.props.update1){
      myLatlng=new google.maps.LatLng(this.props.lat1,this.props.lng1);
    }

    //creating a google map
    var that=this;
    map=new google.maps.Map(this.refs.map, {
      center: myLatlng,
      zoom: 12
    });

    //creating a marker
    marker = new google.maps.Marker({
      map: map,
      draggable:true,
      title:"Drag me!",
    });

    marker.setPosition(myLatlng);

    //checking if component is called from add restaurant page or update page

      //if the component is called from add rstaurant page then call the
      //geolocation apis to get the current location of the user and set it on the map

      if(!this.props.update1){
        var infoWindow = new google.maps.InfoWindow({map: map});
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            pos = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );
            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            map.setCenter(pos);
            if(!that.state.update)
            marker.setPosition(pos);
          }, function() {
            console.log("here");
            var pos = new google.maps.LatLng( 17.411828179914195 , 78.39848885622246 );
            map.setCenter(pos);
            if(!that.state.update)
            marker.setPosition(pos);
            handleLocationError(true, infoWindow, map.getCenter());
          });
        }

        else {
          console.log("there");
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }



    //adding drag event listener for marker
    marker.addListener('dragend', function(e) {
      that.handleLatLng(marker.getPosition());
      map.setCenter(marker.getPosition());
    });
  }

  handleLatLng(v){
    //changing textbox value for latitude and longitude
      document.getElementById('longitude').value=v.lng();
      document.getElementById('latitude').value=v.lat();
      this.setState({lat:v.lat(),lng:v.lng()});
      this.props.onMChange(this.state.lat,this.state.lng);
  }

  setLatLng(e){
    //storing the value of latitude and longitude textbox in state

    this.setState({lat:document.getElementById('latitude').value,lng:document.getElementById('longitude').value});
    var pos = new google.maps.LatLng(this.state.lat,this.state.lng);
    marker.setPosition(pos);
    map.setCenter(pos);
    this.props.onMChange(this.state.lat,this.state.lng);
  }
  componentWillReceiveProps(nextProps){
    if(this.state.prevArea != nextProps.area1)
        this.changeArea(nextProps.area1);
    this.setState({update:nextProps.update1 , prevArea : nextProps.area1});
  }

  changeArea(a){

    var that=this;

    var address = a;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
            map.setCenter(results[0].geometry.location);

        if(that.props.add1){
              marker.setPosition(results[0].geometry.location);
          // that.handleLatLng(results[0].geometry.location);
        }
          } else {
            console.log('Geocode was not successful for the following reason: ' + status);
          }
        });

  }

  handleGeo(e){

    var that = this;

    var a1 = document.getElementById('address2').value
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': a1}, function(results, status) {
      if (status === 'OK') {
            console.log(results[0]);
            map.setCenter(results[0].geometry.location);
            map.setZoom(14);
            marker.setPosition(results[0].geometry.location);
            that.handleLatLng(results[0].geometry.location);
          } else {
            console.log('Geocode was not successful for the following reason: ' + status);
          }
        });
  }

  render() {

    if(this.state.update){
      marker?marker.setPosition(new google.maps.LatLng(this.props.lat1,this.props.lng1)):'';
      map?map.setCenter(new google.maps.LatLng(this.props.lat1,this.props.lng1)):'';
    }

    var vis = this.props.add1?"vis2":"hid2"



    return (
      <div id="MapSample">
        <div id="floating-panel" className = {vis}>
          <input id="address2" type="text"/>
          <button id="submit" className = "btn btn-default" onClick = {this.handleGeo.bind(this)} type="button" ><span className="glyphicon glyphicon-search"></span></button>
        </div>
        <div ref="map" style={{height:500}}></div>
        <form  role="form">
          <div className="form-horizontal" >
              <label className="sr-only">latitude:</label>
              <div className="col-sm-6">
                  <input type="number" placeholder="latitude" id="latitude" className="form-control" onChange={this.setLatLng}/>
              </div>
          </div>
          <div className="form-horizontal " >
              <label className="sr-only">longitude:</label>
              <div className="col-sm-6">
                  <input type="number" placeholder="longitude" id="longitude" className="form-control" onChange={this.setLatLng}/>
              </div>
          </div>
          </form>

      </div>
    );
  }
}

export default MapSample;
