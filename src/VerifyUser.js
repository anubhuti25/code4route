import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link , hashHistory } from 'react-router';
import './App.css';
import './Header.css';
import './VerifyUser.css';

class VerifyUser extends Component {
  constructor() {
    // In a constructor, call `super` first if the class extends another class
    super();
    this.state = {
      j : []
    }
    this.handleLog = this.handleLog.bind(this);
  }

  componentDidMount(){

    var nowRole = ( this.props.params.uname == "oname" ) ? "verify" : "verifyUser";
    var that = this;

    fetch( 'http://localhost:9000/' + nowRole + "?" + that.props.params.uname + "=" + that.props.params.uval ,
    {
      method : 'GET',
      headers :{
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      }
     })
     .then( response => response.json() )
     .then(( json ) => this.setState({ j : json }));
  }

  handleLog(e){
    localStorage.auth_token = this.state.j.auth_token;
    localStorage.role = ( this.props.params.uname == "oname" ) ? "owner" : "user";
    if(localStorage.role == "owner"){
      localStorage.uname = this.state.j.oname;
      console.log(localStorage.uname);
    }
    else {
      localStorage.uname = this.state.j.uname;
    }
    hashHistory.push( '/' )
  }

  render() {

    return (

      <header>
        <div className="container">
          <div className="intro-text" id="verify">
              <div className="intro-heading">HungerQuest</div>
              <div className="intro-lead-in">Your email-id has been successfully verfied. Click this button to login </div>
              <Link to="/">
                <button id="logButton" type="button" className="btn btn-success btn-lg" onClick = { this.handleLog } >Click Here</button>
              </Link>
          </div>
        </div>
      </header>

    );
  }
}

export default VerifyUser;
