import React, { Component } from 'react';
import { Link , hashHistory } from 'react-router';
import Search from './Search';
import Login from './Login.js';
import Signup from './Signup.js';
import './Snav.css';
class Snav extends Component {
  constructor() {
    super();
    this.state = {
      rests:[],
      value:'',
      toggled: false,
      collapsed:true,
      oSign:false
    };
    this.handleSearch=this.handleSearch.bind(this);
    this.selectFilter=this.selectFilter.bind(this);
    this.handleInput=this.handleInput.bind(this);
    this.toggleCollapse=this.toggleCollapse.bind(this);
    this.handleNew=this.handleNew.bind(this);
    this.handleNearBy=this.handleNearBy.bind(this);
    this.closeBox=this.closeBox.bind(this);
    this.signout=this.signout.bind(this);
  }

  closeBox(){
    document.getElementById('profileLi').style.display="block";
    this.forceUpdate();
  }

  signout(){
    var that=this;
    var auth = "Bearer "+localStorage.auth_token;
    // console.log(localStorage.auth_token);
    localStorage.auth_token=''

    var signO=localStorage.role=="owner"?"ownerSignOut":"userSignOut";
    localStorage.role='';

    localStorage.uname='';

    fetch('http://localhost:9000/'+signO,
    {
      method: 'GET',
      headers:{
        "Authorization":auth,
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
     })
     .then(response=>console.log(response.status))
     .then(function(){
       document.getElementById('profileLi').style.display="none";
       localStorage.auth_token='';
       that.setState({oSign:false});
      //  window.location.reload();
      //  window.location.replace(ll);
     });
  }

  handleNearBy(){

    if(this.props.inDisplay){
      hashHistory.push('Listmapview/latlng/1')
    }
    else {
      this.props.nearByRests();
    }
  }

  toggleCollapse() {
      const c= !this.state.collapsed;
      this.setState({collapsed:c});
  }

  handleNew(){

    if(this.props.inDisplay){
      hashHistory.push('Listmapview/new/1')
    }
    else {
      this.props.newRests();
    }
  }

  handleInput(e){
    var val=e.currentTarget.value;
    this.setState({value:val});
  }

  handleSearch(e){

    if(this.props.inDisplay){
      if( this.state.rests.length == 0 || this.state.value == ''){
        hashHistory.push( 'Listmapview/1/1' )
      }
      else {
          hashHistory.push('Listmapview/'+this.state.rests+'/'+this.state.value)
      }
    }
    else {
      this.props.handleSearchFilters(this.state);
    }
    window.location.reload();
  }

  selectFilter(e){
    var param=e.currentTarget.id;
    var val=document.getElementById('search_box').value;
    this.setState({rests:param});
    document.getElementById('search_concept1').innerHTML=""+param;
  }

  componentDidMount(){
    if(localStorage.auth_token){
      document.getElementById('profileLi').style.display="block";
    }

    window.addEventListener('storage', function(e) {
      window.location.reload();
    });
}

displayVis(e){

  var toggle1 = this.state.toggled
  if(!toggle1){
    document.getElementById('filtersCol').style.display
    document.getElementById('filtersCol').className = "col-md-2 col-sm-2 visible-xs";
  }
  else {
    document.getElementById('filtersCol').className = "col-md-2 col-sm-2 hidden-xs";
  }
  this.setState({toggled : !toggle1})


}

componentWillReceiveProps(nextProps){
  if(nextProps.oSignout)
  this.setState({oSign:true})
}

  render() {

    const { collapsed } = this.state;
    const navClass = collapsed ? "collapse" : "";

    var role1 = localStorage.role == "owner" ? "/Owner" : "/User"

    const profileNav=(navClass=="collapse")?<i id="profBtn" className="fa fa-user fa-2x" aria-hidden="true"></i>
                      :<Link to={role1} >Profile</Link>


    const signIn = localStorage.auth_token?'':<Login lists = {true} closeMod1 = {this.closeBox}/>
    const signUp = localStorage.auth_token ? '' : < Signup/>
    const prof=localStorage.auth_token ? profileNav : ''
    const ok = ( this.state.oSign ) ? this.signout() : '';

    var path1 = ( hashHistory.getCurrentLocation().pathname.indexOf('Display') != -1 ) ? "hidden11" : "pull-right visible-xs"
    var FavVis=(localStorage.role=="user")?<Link to = "/User" className="smaller" > Favourites </Link>:"";

    return (
      <nav id="navy" ref="navbar" className="navbar navbar-default navbar-custom">
          <div className="container-fluid">

                  <div className="row" id="wide">
                  <div className = "navbar-header col-sm-2 col-md-2">
                  <button type="button" id= "filtToggle" className="navbar-toggle" onClick={this.toggleCollapse.bind(this)} data-target="#bs-example-navbar-collapse-1">
                      <span className="sr-only">Toggle navigation</span><i className="fa fa-bars"></i>
                  </button>
                          <a href="#" id="haed1" className="navbar-brand">HungerQuest</a>
                          <a className={path1}  id = "filtBtn" onClick = {this.displayVis.bind(this)}><span className="glyphicon glyphicon-search"></span></a>
                      </div>
                      <div id ="custum" className="col-sm-5 col-md-5 hidden-xs">
                		        <div className="input-group">
                                <div className="input-group-btn search-panel">
                                    <button id="search_btn" className="btn btn-default dropdown-toggle"  data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">
                                    	<span id="search_concept1">  </span> <span className="caret"></span>
                                    </button>

                                    <ul id="dd1" className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                      <li><a onClick={this.selectFilter} id="area">Area</a></li>
                                      <li><a onClick={this.selectFilter} id="cuisine">Cuisine</a></li>
                                      <li><a onClick={this.selectFilter} id="name">Name</a></li>
                                    </ul>
                                </div>

                                <input id="search_box" type="text" className="form-control" name="x" placeholder="Search term..." onChange={this.handleInput}  />
                                <span className="input-group-btn ">
                                    <button id="search_icon"  className="btn btn-default" type="button" onClick={this.handleSearch}><span className="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                          </div>


                      <div>

                      <div className={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                          <ul className="nav navbar-nav navbar-right">
                              <li className="hidden">
                                  <a href="#"></a>
                              </li>
                              <li >
                                {signUp}
                              </li>

                              <li >
                                  {signIn}
                              </li>
                              <li >
                                  {FavVis}
                              </li>

                              <li>
                                  <a className="page-scroll smaller" onClick={this.handleNew}>New</a>
                              </li>
                              <li>
                                  <a className="page-scroll smaller" onClick={this.handleNearBy}>Nearby</a>
                              </li>

                              <li  className="smaller hidden-xs" id="profileLi">{(localStorage.auth_token)&&(navClass=="collapse")?<a id="profile">{localStorage.uname}</a>:''}{prof}

                                <ul className="dropdown2" id="dd2">
                                  <li className="link2"><Link to={(localStorage.role=="owner")?"/Owner":"/User"}>My Profile</Link></li>
                                  <hr className="bre"/>
                                  <li className="link2"><Link to={hashHistory.getCurrentLocation().pathname} onClick={this.signout}>Logout</Link></li>
                                </ul>
                              </li>

                              <li className = "visible-xs smaller">
                                  {prof}
                              </li>

                              <li className = "visible-xs smaller">
                                  {localStorage.auth_token?<Link to={hashHistory.getCurrentLocation().pathname} onClick={this.logout}>Logout</Link>:''}
                              </li>

                          </ul>
                      </div>
                      </div>
                  </div>
              </div>

      </nav>
    );
  }
}

export default Snav;
