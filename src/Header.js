import React, { Component } from 'react';
import NowOpen from './NowOpen'
import './Header.css';
import {Collapse} from 'react-bootstrap';
import Search from './Search.js';

class Header extends Component {
  constructor() {
    super();
  }


  render() {

    return (
      <header id="Head1">
        <div className="container">
            <div className="intro-text">
                <div className="intro-lead-in">Explore Your Cravings with</div>
                <div className="intro-heading">HungerQuest</div>
                <Search/>
                <NowOpen/>
            </div>
        </div>
      </header>
    );
  }
}

export default Header;
